/**
 * Created by: smajina December 2018
 */
import { Injectable } from '@angular/core';
import { from, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { environment } from './../../environments/environment';
// import { AuthService } from '../auth/auth.service';
// import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
// import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private accessToken;
  public headers: HttpHeaders;

  constructor(
    private http: HttpClient,
    // private auth: AuthService
    ) {
    // this.auth.getResource()
    if (localStorage.getItem('auth')) this.accessToken = JSON.parse(localStorage.getItem('auth')).access_token;
    this.headers = new HttpHeaders({
      'Accept': 'application/json',
      'Content-type': 'application/json',
      'Authorization': 'Bearer ' + this.accessToken, 'withCredentials': 'true',
      'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Headers': 'X-Requested-With,Content-Type, Accept,Access-Control-Allow-Origin'
    });


  }

  //http get
  getData(url: string): Observable<any> {
      let headers = this.headers;
      //  if(url.indexOf("http:")<0){
        if(window.location.hostname=="127.0.0.1")
        {
          url = 'myhc-api/'+url;
        }
        else
        {
          //url = 'myhc-api/'+url;
          url = environment.apiUrl+url;
          //url = 'myhc-api-dev/'+url;
        }

      //  }
      return this.http.get<any[]>(url, { headers: headers })
        .pipe(map(result => result),
          catchError(err => {
            return throwError(err);
          })
        )
  }


  //http post
  postData(url: string, data: any): Observable<any> {
    // if (this.auth.checkCredentials()) {
      let headers = this.headers;
      if(window.location.hostname=="127.0.0.1")
        {
          url = 'myhc-api/'+url;
        }
        else
        {
          //url = 'myhc-api/'+url;
          url = environment.apiUrl+url;
        }
      return this.http.post(url, data, { headers: headers })
        .pipe(map(result => result,
          catchError(err => {
            return throwError(err);
          })
        ));
    // }
  }




  //http put
  putData(url: string, data: any): Observable<any> {
      let headers = this.headers;
      if(window.location.hostname=="127.0.0.1")
      {
        url = 'myhc-api/'+url;
      }
      else
      {
        url = environment.apiUrl+url;
      }
      return this.http.put(url, data, { headers: headers })
        .pipe(map(result => result,
          catchError(err => {
            return throwError(err);
          })
        ));
  }


  //http delete
  deleteData(url: string): Observable<any> {
      let headers = this.headers;
      if(window.location.hostname=="127.0.0.1")
        {
          url = 'myhc-api/'+url;
        }
        else
        {
          url = environment.apiUrl+url;
        }
      return this.http.delete(url,  { headers, responseType: 'json' })
        .pipe(map(result => result,
          catchError(err => {
            return throwError(err);
          })
        ));
  }

    //http postMultipartData
    public postMultipartHeaders: HttpHeaders;
    postMultipartData(url: string, data: FormData): Observable<any> {
      if(window.location.hostname=="127.0.0.1")
        {
          url = 'myhc-api/'+url;
        }
        else
        {
          url = environment.apiUrl+url;
        }
        const headers2 =   new HttpHeaders({
          'Authorization': 'Bearer ' + this.accessToken, 'withCredentials': 'true',
          //'Content-Type' : 'application/x-www-form-urlencoded',
          //'enctype' : 'application/x-www-form-urlencoded',
          'Content-Type': 'multipart/form-data',
          'enctype': 'multipart/form-data',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'X-Requested-With,Content-Type, Accept,Access-Control-Allow-Origin'
        });
        // 'Content-Type' : 'application/x-www-form-urlencoded'

        const headers = this.postMultipartHeaders;
        return from(fetch(url, {
          method: 'POST',
          headers: {
              'Accept': 'application/json'
          },
          body: data
        }).then((e)=>{
          return e;
        },e=>{
          return e;
        }));
        return this.http.post(url, data, { headers: headers2})
          .pipe(map(result => {
            return result;
          },
          catchError(err => {
            return throwError(err);
          })
        ));
    }

  //http pdf
  downloadPdf(url: string): Observable<any> {
    let headers = new HttpHeaders({
      'Content-type': 'application/pdf;charset=utf-8',
      'Authorization': 'Bearer ' + this.accessToken, 'withCredentials': 'true'
    });
    if(window.location.hostname=="127.0.0.1")
        {
          url = 'myhc-api/'+url;
        }
        else
        {
          url = environment.apiUrl+url;
        }
    return this.http.get(url, { responseType: "blob" });
  }

  viewPdf(url: string): Observable<any> {
    let headers = new HttpHeaders({
      'Content-type': 'application/pdf;charset=utf-8',
      'Authorization': 'Bearer ' + this.accessToken, 'withCredentials': 'true'
    });
    if(window.location.hostname=="127.0.0.1")
        {
          url = 'myhc-api/'+url;
        }
        else
        {
          url = environment.apiUrl+url;
        }
    return this.http.get(url, { responseType: "arraybuffer" });
  }

  //http csv
  downloadCsv(url: string): Observable<any> {
    let headers = new HttpHeaders({
      'Content-type': 'application/csv;charset=utf-8',
      'Authorization': 'Bearer ' + this.accessToken, 'withCredentials': 'true'
    });
    if(window.location.hostname=="127.0.0.1")
        {
          url = 'myhc-api/'+url;
        }
        else
        {
          url = environment.apiUrl+url;
        }
    return this.http.get(url, { responseType: "blob" });
  }
}
