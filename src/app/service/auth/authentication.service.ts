import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
 

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(private http: HttpClient,private router: Router) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('auth')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    login(data) {
      
        return this.http.post<any>(`${environment.apiUrl}/auth/user-authenticate.php`, data)
            .pipe(map(auth => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('auth', JSON.stringify(auth));
                // this.currentUserSubject.next(user);
                return auth;
            }));
    }

    logout() {
        localStorage.clear();
 
        // // remove user from local storage to log user out
        localStorage.removeItem('auth');
        localStorage.removeItem('plan-registration');
        this.currentUserSubject.next(null);
        this.router.navigate[environment.localhost+'/login'];
    }

    relogin(){
        this.router.navigate(['/', 'login']);
    }

 
    
}