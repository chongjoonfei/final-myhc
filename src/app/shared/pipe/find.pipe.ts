import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'find',
})

//smajina 28-feb-2019
@Injectable()
export class FindPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
        if (!items) {
            return [];
        }
        if (!field || !value) {
            return null;
        }

        let list = items.filter(singleItem => {return singleItem[field]==value;})
        if (list.length>0)
            return items.filter(singleItem => {return singleItem[field]==value;});
        else
            return [{}];
    }
 
}