import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AccordionAnchorDirective, AccordionDirective, AccordionLinkDirective} from './accordion';
import {ToggleFullScreenDirective} from './fullscreen/toggle-fullscreen.directive';
import {CardRefreshDirective} from './card/card-refresh.directive';
import {CardToggleDirective} from './card/card-toggle.directive';
import {SpinnerComponent} from './spinner/spinner.component';
import {CardComponent} from './card/card.component';
import {ModalAnimationComponent} from './modal-animation/modal-animation.component';
import {ModalBasicComponent} from './modal-basic/modal-basic.component';
import {DataFilterPipe} from './element/data-filter.pipe';
import {MenuItems} from './menu-items/menu-items';
import {ParentRemoveDirective} from './element/parent-remove.directive';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ClickOutsideModule} from 'ng-click-outside';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCoolDialogsModule } from 'ngx-cool-dialogs';
import { ModalModule, ButtonsModule, BsModalRef, TooltipModule } from 'ngx-bootstrap';
import { DashboardScreeningPlanDetailsComponent } from '../pages/dashboard/screening-plan-details/screening-plan-details.component';
import { DashboardPatientDetailsComponent } from '../pages/dashboard/patient-details/patient-details.component';
import { SafePipe } from './pipe/safe.pipe';

import {CalendarModule} from 'primeng-lts/calendar';
import {AutoCompleteModule} from 'primeng-lts/autocomplete';
import {SidebarModule} from 'primeng-lts/sidebar';
import {ScrollPanelModule} from 'primeng-lts/scrollpanel';
import {TabViewModule} from 'primeng-lts/tabview';
import {FileUploadModule} from 'primeng-lts/fileupload';
import {EditorModule} from 'primeng-lts/editor';

import { DashboardCompanyDetailsComponent } from '../pages/dashboard/company-info/company-details.component';
import { KeysPipe } from './pipe/keys.pipe';
import { ApexChartComponent } from './component/chart/apex-chart/apex-chart.component';
import { ApexChartService } from './component/chart/apex-chart/apex-chart.service';
import { NgxScrollTopModule } from 'ngx-scrolltop';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { SortPipe } from './pipe/sort.pipe';
import { QRCodeModule } from 'angularx-qrcode';
import { ReceiptComponent } from '../pages/receipt/receipt.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    NgbModule ,
    FormsModule,
    ReactiveFormsModule,
    NgxCoolDialogsModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    CalendarModule,
    AutoCompleteModule,
    SidebarModule,
    ScrollPanelModule,
    TabViewModule,
    FileUploadModule,
    EditorModule,
    BsDatepickerModule.forRoot(),
    NgxScrollTopModule,
    QRCodeModule,
    PopoverModule.forRoot()
  ],
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullScreenDirective,
    CardRefreshDirective,
    CardToggleDirective,
    SpinnerComponent,
    CardComponent,
    ModalAnimationComponent,
    ModalBasicComponent,
    DataFilterPipe,
    ParentRemoveDirective,
    DashboardScreeningPlanDetailsComponent,
    DashboardPatientDetailsComponent,
    DashboardCompanyDetailsComponent,
    ReceiptComponent,
    SafePipe,
    KeysPipe,
    SortPipe,
    ApexChartComponent
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ToggleFullScreenDirective,
    CardRefreshDirective,
    CardToggleDirective,
    SpinnerComponent,
    CardComponent,
    ModalAnimationComponent,
    ModalBasicComponent,
    DataFilterPipe,
    ParentRemoveDirective,
    NgbModule,
    PerfectScrollbarModule,
    ClickOutsideModule  ,
    FormsModule,
    ReactiveFormsModule,
    NgxCoolDialogsModule,
    ModalModule,
    DashboardScreeningPlanDetailsComponent,
    DashboardPatientDetailsComponent,
    DashboardCompanyDetailsComponent,
    ReceiptComponent,
    SafePipe,
    TooltipModule,
    CalendarModule,
    AutoCompleteModule,
    KeysPipe,
    SidebarModule,
    ScrollPanelModule,
    ApexChartComponent,
    TabViewModule,
    FileUploadModule,
    EditorModule,
    NgxScrollTopModule,
    BsDatepickerModule,
    SortPipe,
    QRCodeModule,
    PopoverModule
  ],
  providers: [
    BsModalRef,
    MenuItems,
    DatePipe,
    ApexChartService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  schemas: [ NO_ERRORS_SCHEMA ],

})
export class SharedModule { }
