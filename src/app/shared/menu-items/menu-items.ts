import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: '',
    main: [
      {
        state: 'home',
        name: 'HOME',
        type: 'sub',
        icon: 'ti-home',
        children: [
          {
            state: 'dashboard',
            name: 'Dashboard',
            type: 'link'
          } 
        ]
      },
      // {
      //   state: 'dashboard',
      //   short_label: 'D',
      //   name: 'Dashboard',
      //   type: 'link',
      //   icon: 'ti-home'
      // },
      {
        state: 'patient-management',
        name: 'PATIENT MANAGEMENT',
        type: 'sub',
        icon: 'ti-user',
        children: [
          {
            state: 'new-registration',
            name: 'New Registration',
            type: 'link'
          } ,
          {
            state: 'patient-info',
            name: 'Patient Information',
            type: 'link'
          },
          {
            state: 'test-result',
            name: 'Test Result',
            type: 'link'
          } 
        ]
      },
      // {
      //   state: 'patient',
      //   short_label: 'D',
      //   name: 'Patient Info',
      //   type: 'link',
      //   icon: 'ti-user'
      // },
      {
        state: 'test-plan-management',
        name: 'TEST PLAN MANAGEMENT',
        type: 'sub',
        icon: 'fa fa-heartbeat',
        children: [
          {
            state: 'create-new-package',
            name: 'Create New Package',
            type: 'link'
          } ,
          {
            state: 'listing-package',
            name: 'Listing Package',
            type: 'link'
          },
          {
            state: 'promotion',
            name: 'Promotion',
            type: 'link'
          } 
        ]
      },
      {
        state: 'content-management',
        name: 'CONTENT MANAGEMENT',
        type: 'sub',
        icon: 'fa fa-file-text-o',
        children: [
          {
            state: 'post-news',
            name: 'Post News',
            type: 'link'
          } ,
          {
            state: 'article',
            name: 'Article',
            type: 'link'
          } ,
          {
            state: 'video',
            name: 'Video',
            type: 'link'
          },
          {
            state: 'static-pages',
            name: 'Static Pages',
            type: 'link'
          } 
        ]
      },
      {
        state: 'system',
        name: 'SYSTEM',
        type: 'sub',
        icon: 'fa fa-gear',
        children: [
          {
            state: 'settings',
            name: 'Settings',
            type: 'link'
          } ,
        ]
      },
      {
        state: 'reports',
        name: 'REPORTS',
        type: 'sub',
        icon: 'fa fa-print',
        children: [
          {
            state: 'membership',
            name: 'Membership',
            type: 'link'
          } ,
          {
            state: 'sales-plan',
            name: 'Sales (test plan)',
            type: 'link'
          } ,
          {
            state: 'testing-booking',
            name: 'Testing booking',
            type: 'link'
          } ,
          {
            state: 'data-import',
            name: 'Data Import from med.team',
            type: 'link'
          } ,
        ]
      },
      {
        state: 'helpdesk',
        name: 'HELPDESK',
        type: 'link',
        icon: 'fa fa-fax'
      },
     
    ],
  },
  
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
