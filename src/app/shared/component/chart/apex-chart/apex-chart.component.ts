import {Component, Input, OnInit} from '@angular/core';
import ApexCharts from 'apexcharts/dist/apexcharts.common.js';
import {ApexChartService} from './apex-chart.service';

@Component({
  selector: 'app-apex-chart',
  templateUrl: './apex-chart.component.html',
  styleUrls: ['./apex-chart.component.scss']
})
export class ApexChartComponent implements OnInit {
  @Input() chartID: string;
  @Input() chartConfig: any;
  @Input() xAxis: any;
  @Input() newOptions: any;

  public chart: any;

  constructor(private apexEvent: ApexChartService) { }

  ngOnInit() {
    setTimeout(() => {
      this.chart = new ApexCharts(document.querySelector('#' + this.chartID), this.chartConfig);
      this.chart.render();
    });

    this.apexEvent.changeTimeRange.subscribe(() => {
      if (this.xAxis) {
        this.chart.updateOptions({
          xaxis: this.xAxis
        });
      }
    });

    this.apexEvent.changeSeriesData.subscribe(() => {
      //smajina 17-june-2020
      if (this.chartConfig) {
        ApexCharts.exec(this.chartID, "updateOptions", this.chartConfig);
      }
    });
  }

  //smajina 18-june-2020
  ngOnChanges() {
    if (this.chart && this.chartConfig) {
      this.chart.updateOptions(this.chartConfig);
    }
  }



}
