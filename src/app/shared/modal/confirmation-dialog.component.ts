import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-confirmation-dialog',
    templateUrl: 'confirmation-dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  export class ConfirmationDialogComponent {
    title: string;
    prompt: string;

    constructor(
      public activeModal: NgbActiveModal,
      public translate: TranslateService
    ) {
      if (localStorage.getItem('lang')) {
        this.translate.use(localStorage.getItem('lang'));
      } else {
        this.translate.setDefaultLang('en');
      }
    }
  }
