import { Injectable } from '@angular/core';

import {
  NgbModal,
  NgbModalRef
} from '@ng-bootstrap/ng-bootstrap';
import { Observable, from, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';



@Injectable({
  providedIn: 'root'
})
export class ConfirmationModalService {

  modalReference: NgbModalRef;
  closeResult: any;

  constructor(
    private ngbModal: NgbModal
  ) { }


  confirm(
    prompt = 'Really?', title = 'Confirm'
  ): Observable<boolean> {
    const modal = this.ngbModal.open(
      ConfirmationDialogComponent, { backdrop: 'static' });

    modal.componentInstance.prompt = prompt;
    modal.componentInstance.title = title;

    return from(modal.result).pipe(
      catchError(error => {
        console.warn(error);
        return of(undefined);
      })
    );
  }
}
