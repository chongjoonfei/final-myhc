import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './shared/material-module';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { NgxScrollTopModule } from 'ngx-scrolltop';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { NgApexchartsModule } from 'ng-apexcharts';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ConfirmationDialogComponent } from './shared/modal/confirmation-dialog.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MainComponent } from './layout/main/main.component';
import { BreadcrumbsComponent } from './layout/main/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layout/main/title/title.component';
import { AuthComponent } from './layout/auth/auth.component';
import { SharedModule} from './shared/shared.module';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DataService } from './service/data.service';
import { CustomLinerChartService } from './service/CustomLinerChartService';
import { PatientModule } from './pages/patient/patient.module';
import { AddOnPurchaseModule } from './pages/add-on/add-on.module';
import { CorporateModule } from './pages/corporate/corporate.module';
import { DependentModule } from './pages/dependent/dependent.module';
import { MauPageModule } from './pages/mau-page/mau-page.module';
import { AuthenticationService } from './service/auth/Authentication.service';
import { MarketplaceModule } from './pages/marketplace/marketplace.module';
import { MyDashboardComponent } from './pages/my-dashboard/my-dashboard.component';
import { MyBiodataComponent } from './pages/my-biodata/my-biodata.component';
import { MyQrCodeComponent } from './pages/my-qr-code/my-qr-code.component';
import { AuthInterceptor } from './service/auth/AuthInterceptor';
import { ErrorComponent } from './pages/error/error.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { MyAssessmentsComponent } from './pages/assessments/my-assessments/my-assessments.component';
import { MyRecordsComponent } from './pages/records/my-records/my-records.component';
import { MyBookingComponent } from './pages/my-booking/my-booking.component';


export function HttpLoaderFactory(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    {prefix: "./assets/i18n/auth/", suffix: ".json"},
    {prefix: "./assets/i18n/admin/", suffix: ".json"},
    {prefix: "./assets/i18n/member-mgmt/", suffix: ".json"},
    {prefix: "./assets/i18n/my-dashboard/", suffix: ".json"},
    {prefix: "./assets/i18n/marketplace/", suffix: ".json"},
    {prefix: "./assets/i18n/my-account/", suffix: ".json"},
    {prefix: "./assets/i18n/my-biodata/", suffix: ".json"},
    {prefix: "./assets/i18n/my-qr-code/", suffix: ".json"},
    {prefix: "./assets/i18n/screening-test/", suffix: ".json"},
    {prefix: "./assets/i18n/test-panel/", suffix: ".json"},
    {prefix: "./assets/i18n/my-booking/", suffix: ".json"},
    {prefix: "./assets/i18n/app/", suffix: ".json"},
  ]);
}

@NgModule({
  declarations: [
    ConfirmationDialogComponent,
    AppComponent,
    MainComponent,
    BreadcrumbsComponent,
    TitleComponent,
    AuthComponent ,
    MyDashboardComponent,
    MyBiodataComponent,
    MyQrCodeComponent,
    ErrorComponent,
    MyAccountComponent,
    MyAssessmentsComponent,
    MyRecordsComponent,
    MyBookingComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgApexchartsModule,
    AppRoutingModule,
    SharedModule,
    PatientModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    SlickCarouselModule,
    MaterialModule,
    AddOnPurchaseModule,
    CorporateModule,
    DependentModule,
    NgxScrollTopModule,
    MauPageModule,
    MarketplaceModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  exports: [
    TranslateModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    DataService,
    CustomLinerChartService,
    TranslateService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
