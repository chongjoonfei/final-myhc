import { Input, TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import * as _ from "lodash";

import { DataService } from '../../service/data.service';
import { AuthenticationService } from '../../service/auth/Authentication.service';

import { environment } from '../../../environments/environment';
import { DatePipe } from '@angular/common';

declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: [
    './my-account.component.scss'
  ]
})
export class MyAccountComponent implements OnInit {


  constructor(
    private router: Router,
    private toastrService: ToastrService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    private authenticationService : AuthenticationService,
    private datePipe: DatePipe
  ) { }

  modalRef: BsModalRef;
  @Input('data-registration') dataRegistration: any;

  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    "infinite": false
  };

  user: any;
  auth: any;
  userAccount: any;
  dataPatients: any;
  staffPatient: any;
  dataSharing: any;
  dataSharingError={};
  loading = true;
  uploading = false;
  photoPath: any;
  ngOnInit() {

    this.photoPath = environment.uploadPath;
    this.fetchlookupInfo();
    this.auth = JSON.parse(localStorage.getItem("auth"));

    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

    if (this.auth.account.acc_type_code == 'ADMIN') {
      this.dataService.getUserDetails(this.auth.username).subscribe(user => {

        this.user = user;
        this.fetchRegistrationInfo();
      }, error => {
        // this.loading = false;
      });
    } else {

      this.dataService.getUserAccount(this.auth.account.ic_no).subscribe(res=>{

      this.userAccount = _.filter(res.registration, { 'package_code': this.dataRegistration.package_code });

      let user = _.filter(this.userAccount[0].registration_persons, { 'ic_no': this.auth.account.ic_no });
      this.user = user[0];

      this.dataService.getDataSharing("?ic="+this.user.person.ic_no).subscribe(res=>{
        this.dataSharingError=false;
        this.dataSharing=res;
      },(e)=>{
        this.dataSharingError=true;
        console.log(e);
      });

      // console.log("user patient details",user);
      this.fetchRegistrationInfo();
    });
    }


    this.loading = false;
  }

  addRegPersonNote: any;
  totalPatient = 0;
  selectedPatientData: any;
  dataMAU: any;
  qrActive = 0;
  fetchRegistrationInfo() {
    this.totalPatient = 0;
    this.addRegPersonNote = null;
    this.loading = false;

    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE') this.staffPatient = 'staff'; else this.staffPatient = 'patient';
    // this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });

    if (this.auth.account.acc_type_code != 'ADMIN' && this.user.user_account.acc_type_code == 'STAFF') {
      this.dataPatients = _.filter(this.userAccount[0].registration_persons, { 'ic_no': this.user.ic_no });
      
    } else {
      this.dataPatients = _.filter(this.userAccount[0].registration_persons, { 'person_type_code': 'PATIENT' });
    }

    this.dataMAU = _.filter(this.userAccount[0].registration_persons, { 'admin_type': 'MAU' });
    this.selectedPatientData = this.dataPatients[this.qrActive];

    //console.log( this.selectedPatientData + " LLLL");


    this.userAccount[0].screening_plan.package_patients.forEach(element => {
      this.totalPatient = this.totalPatient + Number(element.total_patient);
      if (this.addRegPersonNote != null)
        this.addRegPersonNote = this.addRegPersonNote + ' and ' + element.total_patient + " " + element.patient_type_code;
      else
        this.addRegPersonNote = element.total_patient + " " + element.patient_type_code;
    });
    this.isSubmitted = false;
    // this.loading = false;
    // console.log(this.dataRegistration);

  }

  setDefaultPic(event) {
    event.target.src = "assets/images/avatar.jpg";
  }

  states: any;
  relationships: any;
  fetchlookupInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
    this.dataService.getRelationships().subscribe(res => {
      this.relationships = res.data;
      // console.log(this.relationships);
    });
  }

  selectedCity= "";
  getSelectedCity(postcode) {
    this.dataService.getSelectedCity(postcode).subscribe(res => {
      this.selectedCity = res[0].post_office;
    }, error => {
      this.selectedCity = "";
      this.translate.get('registration').subscribe(data => {
        this.toastrService.error(data.notification.title_errorCity, data.notification.title_warning);
      });
    });
  }


  showProfile(index) {
    this.qrActive = index;
    this.selectedPatientData = this.dataPatients[index];
  }

  newPatient(template: TemplateRef<any>) { //default info based on MAU for Family
    let mau = null;
    if (this.dataMAU && this.dataMAU[0]) mau = this.dataMAU[0];
    this.mode = "NEW";

    if (this.dataRegistration.screening_plan.category_code == 'FAMILY' || this.dataRegistration.screening_plan.category_code == 'DEPENDENT') {
      this.dataForm = this.formBuilder.group({
        name: [null, Validators.required],
        age: [null, Validators.required],
        ic_no: [null, Validators.required],
        mobile_no: [mau ? mau.person.mobile_no : null, Validators.required],
        email: [mau ? mau.person.email : null, [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        gender: [null, Validators.required],
        patient_type_code: null,
        address: [mau ? mau.person.address : null, Validators.required],
        town: [mau ? mau.person.town : null, Validators.required],
        district: mau ? mau.person.district : null,
        postcode: [mau ? mau.person.postcode : null, Validators.required],
        state: [mau ? mau.person.state : null, Validators.required],
        picture_path: null,
        status: null,
        relationship: null,
        reg_no: this.dataRegistration.reg_no
      });
    } else {
      this.dataForm = this.formBuilder.group({
        name: [null, Validators.required],
        age: [null, Validators.required],
        ic_no: [null, Validators.required],
        mobile_no: [null, Validators.required],
        email: [null, [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        gender: [null, Validators.required],
        patient_type_code: null,
        address: null,
        town: null,
        district: null,
        postcode: null,
        state: null,
        picture_path: null,
        status: null,
        relationship: null,
        reg_no: this.dataRegistration.reg_no
      });
    }
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  mode: any;
  dataForm: FormGroup;
  citizen: any;
  editPatient(template: TemplateRef<any>, data) {
    console.log(data);
   
    this.mode = "EDIT";
    this.dataForm = this.formBuilder.group({
      name: [data.name, Validators.required],
      age: [data.age, Validators.required],
      ic_no: [data.ic_no, Validators.required],
      mobile_no: [data.mobile_no, Validators.required],
      email: [data.email, [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      gender: [data.gender, Validators.required],
      citizen: [data.citizen],
      race: [data.race, Validators.required],
      religion: [data.religion, Validators.required],
      marital: [data.marital],
      dob: [data.date_of_birth, Validators.required],
      patient_type_code: [data.patient_type_code],
      address: [data.address, Validators.required],
      town: [data.town, Validators.required],
      district: [data.district],
      postcode: [data.postcode, Validators.required],
      state: [data.state, Validators.required],
      status: [data.status],
      relationship: [data.relationship],
      reg_no: this.dataRegistration.reg_no
    });
    this.selectedCity = data.district;
    this.citizen = (data.citizen == '' || data.citizen == 'false') ? false : true;
    if (this.dataRegistration.screening_plan.category_code == 'FAMILY' || this.dataRegistration.screening_plan.category_code == 'DEPENDENT') {
      this.dataForm.get('relationship').setValidators(Validators.required);
    }
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  isSubmitted = false;
  savePatient() {
    this.dataForm.value.dob = this.datePipe.transform(this.dataForm.value.dob, 'yyyy-MM-dd hh:mm a');

    this.isSubmitted = true;
    if (this.dataForm.invalid) {
      this.translate.get('my-account').subscribe(lang => {
        this.toastrService.warning(lang.accordion.accountInfo.notification.title_incompletedDesc, lang.accordion.accountInfo.notification.title_incompleted);
      });
      return false;
    }

    if (this.mode == "NEW") {
      if (!this.verifyPatientType('save')) {
        return;
      }
      this.translate.get('my-account').subscribe(lang => {
        this.coolDialogs.confirm(lang.accordion.accountInfo.notification.title_confirmRegister + "?")
        .subscribe(res => {
          if (res) {
            let person = this.dataForm.value;
            let regPerson = { ic_no: person.ic_no, reg_no: this.dataRegistration.reg_no, person_type_code: "PATIENT", admin_type: "NONE", patient_type_code: person.age > 18 ? 'ADULT' : 'CHILD' };

            let data = {
              "person": person,
              "registration": regPerson
            };
            this.dataService.registerPerson(data).subscribe(reg => {
              this.toastrService.success(reg.message, lang.accordion.accountInfo.notification.title_success);
              this.refreshRegistrationInfo();
              this.modalRef.hide();
            }, msg1 => {
              this.toastrService.error(msg1.error.message, lang.accordion.accountInfo.notification.title_failed);
            });
          }
        });
      });
    } else {
     
      this.translate.get('my-account').subscribe(lang => {
      this.coolDialogs.confirm(lang.accordion.accountInfo.notification.title_updateRegister + "?")
        .subscribe(res => {
          this.dataForm.value.citizen = (this.dataForm.value.citizen == '' || this.dataForm.value.citizen == false) ? 'false' : 'true';
          if (res) {
            let person = this.dataForm.value;
            console.log(person);
            this.dataService.saveUpdatePerson(person).subscribe(reg => {
              //console.log(reg);
              this.toastrService.success(reg.message, lang.accordion.accountInfo.notification.title_success);
             // this.router.navigate(['/my-account/main']);
             //alert("sohai");
              //this.refreshRegistrationInfo();
              this.ngOnInit();

              this.modalRef.hide();
            }, msg1 => {
              this.toastrService.error(msg1.error.message, lang.accordion.accountInfo.notification.title_failed);
            });
          }
        });
      });
    }
  }

  refreshRegistrationInfo() {
    this.dataService.getRegistrationInfo(this.dataRegistration.reg_no).subscribe(res => {
      this.dataRegistration = res;

      this.fetchRegistrationInfo();
    });
  }

  verifyPatientType(action) {
    let isValid = true;
    let msg = "";
    let patient_type_code = null;

    if (action == 'save') {
      let person = this.dataForm.value;
      person.patient_type_code = this.getAgePatientTypeCode(person.age);
      patient_type_code = person.patient_type_code;
    }

    // console.log("person",person);

    let cPatient = _.countBy(this.dataPatients, 'patient_type_code');

    Object.entries(cPatient).forEach(([key, value]) => {
      // console.log(key,value);
      let totalPatient = value as number;
      let plan = _.filter(this.dataRegistration.screening_plan.package_patients, { patient_type_code: key });


      if (plan.length > 0) {

        //  console.log(totalPatient + " > " + plan[0].total_patient);
        if (key == patient_type_code && action == 'save') totalPatient = totalPatient + 1;
        if (totalPatient > plan[0].total_patient) {
          isValid = false;
          msg += "\nTotal " + key + " should not be more than " + plan[0].total_patient + " person(s)";
          return;
        }
      }
    });
    // console.log(isValid,msg);
    if (!isValid) this.toastrService.warning(msg, "Warning");
    return isValid;
  }

  getAgePatientTypeCode(age) {
    if (age < 18) return "CHILD";
    else if (age <= 60) return "ADULT";
    else if (age > 60) return "ELDERLY";
  }

  docUrl: any;
  printDoc(url) {

    this.docUrl = environment.uploadPath + url;
    console.log(this.docUrl);
    var proxyIframe = document.createElement('iframe');
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(proxyIframe);
    proxyIframe.style.width = '100%';
    proxyIframe.style.height = '100%';
    proxyIframe.style.display = 'none';

    var contentWindow = proxyIframe.contentWindow;
    contentWindow.document.open();
    contentWindow.document.write('<iframe src="' + this.docUrl + '" onload="print();" width="1000" height="1800" frameborder="0" marginheight="0" marginwidth="0">');
    contentWindow.document.close();
  }

  docUploadList: any;
  selectedRegPerson: any;
  uploadDoc(template: TemplateRef<any>, reg) {
    this.uploadedFiles = [];
    this.dataService.getDocUploadTypes(this.dataRegistration.screening_plan.category_code).subscribe(res => {
      this.docUploadList = res.data;
      // this.matchPersonWithUploadDocs(reg.person.documents);

    });

    this.selectedRegPerson = reg;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-l' });
  }

  uploadedFiles: any[] = [];
  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
  }

  uploadDocNow(event, pfileUpload) {
    this.uploadedFiles = [];
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
    //console.log("upload", this.uploadedFiles);
    this.coolDialogs.confirm("Are you sure to upload this file?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('ic_no', this.selectedRegPerson.person.ic_no);
          formData.append('document_code', 'all');
          // Array.from(this.uploadedFiles).forEach(f => formData.append('fileToUpload', f));
          for (var i = 0; i < this.uploadedFiles.length; i++) {
            formData.append("fileToUpload[]", this.uploadedFiles[i]);
          }

          this.dataService.uploadSupportingDocV2(formData).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error(res.message, "Failed");
            } else {
              this.uploadedFiles = [];
              pfileUpload.clear();
              this.toastrService.success("Document has been successfully uploaded", "Success");
              //this.fetchPerson();
            }
            this.uploading = false;
          }, error => {
            this.uploading = false;
          });
        }
      });
  }

  removeFile(personDoc) {
    this.translate.get('my-account').subscribe(lang => {
    this.coolDialogs.confirm(lang.accordion.accountInfo.notification.title_removeUpload + " " + personDoc.ori_filename + "?")
      .subscribe(res => {
        this.dataService.removePersonDocument(personDoc).subscribe(res => {
          if (res.errorFound) {
            this.toastrService.error(lang.accordion.accountInfo.notification.title_errorRemove, lang.accordion.accountInfo.notification.title_failed);
          } else {
            this.toastrService.success(lang.accordion.accountInfo.notification.title_remove, lang.accordion.accountInfo.notification.title_success);
          }
        }, error => {
        });
      });
    });
  }

  photoFile: any[] = [];
  tmpPhotoFile: any = [];
  uploadPhotoNow(event, pPhoto) {
    // console.log("uploadPhotoNow", event);
    this.photoFile = [];
    for (let file of event.files) {
      this.photoFile.push(file);
    }
    // console.log("upload", this.uploadedFiles);
    this.translate.get('my-account').subscribe(lang => {
    this.coolDialogs.confirm(lang.accordion.accountInfo.notification.title_confirmPhoto + "?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('ic_no', this.selectedRegPerson.person.ic_no);
          formData.append('document_code', 'photo');
          for (var i = 0; i < this.photoFile.length; i++) {
            formData.append("fileToUpload[]", this.photoFile[i]);
          }
          this.dataService.uploadSupportingDocV2(formData).subscribe(res => {
            if (res.errorFound) {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.error(res.message, "Failed");
            } else {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.success(lang.accordion.accountInfo.notification.title_uploadPhoto, lang.accordion.accountInfo.notification.title_success);
            }
            this.uploading = false;
          }, error => {
            this.uploading = false;
          });
        }
      });
    });
  }

  completeMyRegistration() {
    if (!this.verifyPatientType('complete')) {
      return;
    }

    if (this.dataRegistration.screening_plan.package_patients.length != this.dataPatients.length && !this.dataRegistration.company.co_reg_no) {
      this.translate.get('my-account').subscribe(lang => {
        this.toastrService.warning(lang.accordion.accountInfo.notification.title_incompletedDesc, lang.accordion.accountInfo.notification.title_incompleted);
      });
      return;
    }

    this.translate.get('my-account').subscribe(lang => {
    this.coolDialogs.confirm(lang.accordion.accountInfo.notification.title_tnc)
      .subscribe(confirm => {
        if (confirm) {
          let status = "PENDING-APPROVAL";
          let data = { "reg_no": this.dataRegistration.reg_no, "field_name": "status", "field_value": status }
          this.dataService.updateRegField(data).subscribe(res => {
            if (!res.errorFound) {
              this.dataRegistration.status = status;
              this.toastrService.success(lang.accordion.accountInfo.notification.title_pendingApproval, lang.accordion.accountInfo.notification.title_success);
            } else {
              this.toastrService.error(lang.accordion.accountInfo.notification.title_failedSubmit, lang.accordion.accountInfo.notification.title_failed);
            }
          }, error => {
            this.toastrService.error(lang.accordion.accountInfo.notification.title_failedSubmit, lang.accordion.accountInfo.notification.title_failed);
          });
        }
      });
    });
  }

  removeRegPerson(patient) {
    this.translate.get('my-account').subscribe(lang => {
    this.coolDialogs.confirm(lang.accordion.accountInfo.notification.title_removePatient + " " + patient.person.name + "?")
      .subscribe(res => {
        if (res) {
          this.dataService.removePatient(patient).subscribe(reg => {
            this.toastrService.success(reg.message, lang.accordion.accountInfo.notification.title_success);
            this.refreshRegistrationInfo();
          }, msg1 => {
            this.toastrService.error(msg1.error.message, lang.accordion.accountInfo.notification.title_failed);
          });
        }
      });
    });
  }

  resetPassword(user) {
    this.translate.get('my-account').subscribe(lang => {
    this.coolDialogs.confirm(lang.accordion.accountInfo.notification.title_resetPassword + " " + user.person.name + "?")
      .subscribe(confirm => {
        if (confirm) {
          let data = { "ic_no": user.user_account.ic_no, "username": user.user_account.username };

          this.dataService.resetPassword(data).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error(lang.accordion.accountInfo.notification.title_failedReset, lang.accordion.accountInfo.notification.title_failed);
            } else {
              this.toastrService.success(lang.accordion.accountInfo.notification.title_successReset, lang.accordion.accountInfo.notification.title_success);
            }
          }, error => {
            this.toastrService.error(lang.accordion.accountInfo.notification.title_failedReset, lang.accordion.accountInfo.notification.title_failed);
          });
        }
      });
    });
  }

  cpass:any;
  npass:any;
  rpass:any;
  changePassword(){
      if (this.npass!=this.rpass){
        this.translate.get('my-account').subscribe(lang => {
          this.toastrService.warning(lang.accordion.accountInfo.notification.title_notMatchDesc, lang.accordion.accountInfo.notification.title_notMatch);
        });
      }else{
          let data = { "username": this.user.user_account.username, "npass": this.npass, "cpass": this.cpass };
          console.log(this.user.user_account.username);
          this.translate.get('my-account').subscribe(lang => {
            this.dataService.changePassword(data).subscribe(res => {
              this.toastrService.success(lang.accordion.accountInfo.notification.title_successUpdate, lang.accordion.accountInfo.notification.title_success);
              this.npass=null;
              this.rpass=null;
              this.authenticationService.logout()
            }, msg1 => {
              this.toastrService.error(msg1.error.message, lang.accordion.accountInfo.notification.title_failed);
            });
          });
      }
  }


}
