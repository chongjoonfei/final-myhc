import { Input, TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-dsh-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: [
    './patient-details.component.scss'
  ]
})
export class DashboardPatientDetailsComponent implements OnInit {



  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    private toastrService: ToastrService,
    private route: ActivatedRoute
  ) { }

  modalRef: BsModalRef;
  @Input('data-registration') dataRegistration: any;

  user: any;
  auth: any;
  dataPatients: any;
  staffPatient: any;
  loading = true;
  photoPath: any;
  ngOnInit() {
    this.photoPath = environment.uploadPath;
    this.fetchlookupInfo();
    this.auth = JSON.parse(localStorage.getItem("auth"));
    if (this.auth.account.acc_type_code == 'ADMIN') {
      this.dataService.getUserDetails(this.auth.username).subscribe(user => {
        this.user = user;
        this.fetchRegistrationInfo();
      }, error => {
        // this.loading = false;
      });
    } else {
      let user = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.auth.account.ic_no });
      this.user = user[0];
      this.fetchRegistrationInfo();
    }


    this.loading = false;
  }

  addRegPersonNote: any;
  totalPatient = 0;
  dataMAU: any;
  fetchRegistrationInfo() {
    this.totalPatient = 0;
    this.addRegPersonNote = null;
    this.loading = false;
    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE') this.staffPatient = 'staff'; else this.staffPatient = 'patient';

    if (this.auth.account.acc_type_code != 'ADMIN' && this.user.user_account.acc_type_code == 'STAFF') {
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.user.ic_no });
    } else {
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
    }

    this.dataMAU = _.filter(this.dataRegistration.registration_persons, { 'admin_type': 'MAU' });

    this.dataRegistration.screening_plan.package_patients.forEach(element => {
      this.totalPatient = this.totalPatient + Number(element.total_patient);
      if (this.addRegPersonNote != null)
        this.addRegPersonNote = this.addRegPersonNote + ' and ' + element.total_patient + " " + element.patient_type_code;
      else
        this.addRegPersonNote = element.total_patient + " " + element.patient_type_code;
    });
    this.isSubmitted = false;
    // this.loading = false;
    // console.log(this.dataRegistration);
  }


  refreshRegistrationInfo() {
    this.dataService.getRegistrationInfo(this.dataRegistration.reg_no).subscribe(res => {
      this.dataRegistration = res;
      this.fetchRegistrationInfo();
    });
  }

  states: any;
  relationships: any;
  fetchlookupInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
    this.dataService.getRelationships().subscribe(res => {
      this.relationships = res.data;
      // console.log(this.relationships);
    });
  }


  mode: any;
  dataForm: FormGroup;
  editPatient(template: TemplateRef<any>, data) {
    console.log(data);
    this.mode = "EDIT";
    this.dataForm = this.formBuilder.group({
      name: [data.name, Validators.required],
      age: [data.age, Validators.required],
      ic_no: [data.ic_no, Validators.required],
      mobile_no: [data.mobile_no, Validators.required],
      email: [data.email, [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      gender: [data.gender, Validators.required],
      patient_type_code: [data.patient_type_code],
      address: [data.address, Validators.required],
      town: [data.town, Validators.required],
      district: [data.district],
      postcode: [data.postcode, Validators.required],
      state: [data.state, Validators.required],
      status: [data.status],
      relationship: [data.relationship],
      reg_no: this.dataRegistration.reg_no
    });
    if (this.dataRegistration.screening_plan.category_code == 'FAMILY' || this.dataRegistration.screening_plan.category_code == 'DEPENDENT') {
      this.dataForm.get('relationship').setValidators(Validators.required);
    }
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  newPatient(template: TemplateRef<any>) { //default info based on MAU for Family
    let mau = null;
    if (this.dataMAU && this.dataMAU[0]) mau = this.dataMAU[0];
    this.mode = "NEW";

    if (this.dataRegistration.screening_plan.category_code == 'FAMILY' || this.dataRegistration.screening_plan.category_code == 'DEPENDENT') {
      this.dataForm = this.formBuilder.group({
        name: [null, Validators.required],
        age: [null, Validators.required],
        ic_no: [null, Validators.required],
        mobile_no: [mau ? mau.person.mobile_no : null, Validators.required],
        email: [mau ? mau.person.email : null, [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        gender: [null, Validators.required],
        patient_type_code: null,
        address: [mau ? mau.person.address : null, Validators.required],
        town: [mau ? mau.person.town : null, Validators.required],
        district: mau ? mau.person.district : null,
        postcode: [mau ? mau.person.postcode : null, Validators.required],
        state: [mau ? mau.person.state : null, Validators.required],
        picture_path: null,
        status: null,
        relationship: null,
        reg_no: this.dataRegistration.reg_no
      });
    } else {
      this.dataForm = this.formBuilder.group({
        name: [null, Validators.required],
        age: [null, Validators.required],
        ic_no: [null, Validators.required],
        mobile_no: [null, Validators.required],
        email: [null, [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        gender: [null, Validators.required],
        patient_type_code: null,
        address: null,
        town: null,
        district: null,
        postcode: null,
        state: null,
        picture_path: null,
        status: null,
        relationship: null,
        reg_no: this.dataRegistration.reg_no
      });
    }
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  docUploadList: any;
  selectedRegPerson: any;
  uploadDoc(template: TemplateRef<any>, reg) {
    this.uploadedFiles = [];
    this.dataService.getDocUploadTypes(this.dataRegistration.screening_plan.category_code).subscribe(res => {
      this.docUploadList = res.data;
      // this.matchPersonWithUploadDocs(reg.person.documents);

    });

    this.selectedRegPerson = reg;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-l' });
  }

  resetPassword(user) {
    this.coolDialogs.confirm("Are you sure to reset password for " + user.person.name + "?")
      .subscribe(confirm => {
        if (confirm) {
          let data = { "ic_no": user.user_account.ic_no, "username": user.user_account.username };

          this.dataService.resetPassword(data).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error("Unable to reset user password", "Failed");
            } else {
              this.toastrService.success("User password has been reset", "Success");
            }
          }, error => {
            this.toastrService.error("Unable to reset user password", "Failed");
          });
        }
      });
  }

  uploading = false;
  // uploadPhoto(files: File[], doc_code) {
  //   this.coolDialogs.confirm("Are you sure to upload photo?")
  //     .subscribe(res => {
  //       if (res) {
  //         this.uploading = true;
  //         var formData = new FormData();
  //         formData.append('ic_no', this.selectedRegPerson.person.ic_no);
  //         formData.append('document_code', 'photo');
  //         Array.from(files).forEach(f => formData.append('fileToUpload', f));
  //         this.dataService.uploadSupportingDoc(formData).subscribe(res => {
  //           if (res.errorFound) {
  //             this.toastrService.error(res.message, "Failed");
  //           } else {
  //             this.toastrService.success("Document has been successfully uploaded", "Success");
  //             this.fetchPerson();
  //           }
  //           this.uploading = false;
  //         }, error => {
  //           this.uploading = false;
  //         });
  //       }
  //     });
  // }

  uploadedFiles: any[] = [];
  onUpload(event) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
  }

  uploadDocNow(event, pfileUpload) {
    this.uploadedFiles = [];
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
    // console.log("upload", this.uploadedFiles);
    this.coolDialogs.confirm("Are you sure to upload this file?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('ic_no', this.selectedRegPerson.person.ic_no);
          formData.append('document_code', 'all');
          // Array.from(this.uploadedFiles).forEach(f => formData.append('fileToUpload', f));
          for (var i = 0; i < this.uploadedFiles.length; i++) {
            formData.append("fileToUpload[]", this.uploadedFiles[i]);
          }
          this.dataService.uploadSupportingDocV2(formData).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error(res.message, "Failed");
            } else {
              this.uploadedFiles = [];
              pfileUpload.clear();
              this.toastrService.success("Document has been successfully uploaded", "Success");
              this.fetchPerson();
            }
            this.uploading = false;
          }, error => {
            this.uploading = false;
          });
        }
      });
  }

  photoFile: any[] = [];
  tmpPhotoFile: any = [];
  uploadPhotoNow(event, pPhoto) {
    // console.log("uploadPhotoNow", event);
    this.photoFile = [];
    for (let file of event.files) {
      this.photoFile.push(file);
    }
    // console.log("upload", this.uploadedFiles);
    this.coolDialogs.confirm("Are you sure to upload this photo?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('ic_no', this.selectedRegPerson.person.ic_no);
          formData.append('document_code', 'photo');
          for (var i = 0; i < this.photoFile.length; i++) {
            formData.append("fileToUpload[]", this.photoFile[i]);
          }
          this.dataService.uploadSupportingDocV2(formData).subscribe(res => {
            if (res.errorFound) {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.error(res.message, "Failed");
            } else {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.success("Photo has been successfully uploaded", "Success");
              this.fetchPerson();
            }
            this.uploading = false;
          }, error => {
            this.uploading = false;
          });
        }
      });
  }

  verifyPatientType(action) {
    let isValid = true;
    let msg = "";
    let patient_type_code = null;

    if (action == 'save') {
      let person = this.dataForm.value;
      person.patient_type_code = this.getAgePatientTypeCode(person.age);
      patient_type_code = person.patient_type_code;
    }

    // console.log("person",person);

    let cPatient = _.countBy(this.dataPatients, 'patient_type_code');

    Object.entries(cPatient).forEach(([key, value]) => {
      // console.log(key,value);
      let totalPatient = value as number;
      let plan = _.filter(this.dataRegistration.screening_plan.package_patients, { patient_type_code: key });


      if (plan.length > 0) {

        //  console.log(totalPatient + " > " + plan[0].total_patient);
        if (key == patient_type_code && action == 'save') totalPatient = totalPatient + 1;
        if (totalPatient > plan[0].total_patient) {
          isValid = false;
          msg += "\nTotal " + key + " should not be more than " + plan[0].total_patient + " person(s)";
          return;
        }
      }
    });
    // console.log(isValid,msg);
    if (!isValid) this.toastrService.warning(msg, "Warning");
    return isValid;
  }

  fetchPerson() {
    this.dataService.getPerson(this.selectedRegPerson.person.ic_no).subscribe(res => {
      this.selectedRegPerson.person = res;
      // this.matchPersonWithUploadDocs(res.data);
    });
  }

  getAgePatientTypeCode(age) {
    if (age < 18) return "CHILD";
    else if (age <= 60) return "ADULT";
    else if (age > 60) return "ELDERLY";
  }

  // matchPersonWithUploadDocs(personDocuments) {
  //   let docUploaded = [];
  //   this.docUploadList.forEach(doc => {
  //     doc.uploaded = [];
  //     if (personDocuments.length > 0) {
  //       docUploaded = _.filter(personDocuments, { 'document_code': doc.code });
  //       doc.uploaded = docUploaded;
  //     }
  //   });
  // }

  removeFile(personDoc) {
    this.coolDialogs.confirm("Are you sure to remove " + personDoc.ori_filename + "?")
      .subscribe(res => {
        this.dataService.removePersonDocument(personDoc).subscribe(res => {
          if (res.errorFound) {
            this.toastrService.error("Sorry, there was an error removing your file.", "Failed");
          } else {
            this.fetchPerson();
            this.toastrService.success("The selected file has been removed", "Success");
          }
        }, error => {
          this.fetchPerson();
        });
      });
  }

  docUrl: any;
  printDoc(url) {

    this.docUrl = environment.uploadPath + url;
    console.log(this.docUrl);
    var proxyIframe = document.createElement('iframe');
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(proxyIframe);
    proxyIframe.style.width = '100%';
    proxyIframe.style.height = '100%';
    proxyIframe.style.display = 'none';

    var contentWindow = proxyIframe.contentWindow;
    contentWindow.document.open();
    contentWindow.document.write('<iframe src="' + this.docUrl + '" onload="print();" width="1000" height="1800" frameborder="0" marginheight="0" marginwidth="0">');
    contentWindow.document.close();
  }

  isSubmitted = false;
  savePatient() {

    this.isSubmitted = true;
    if (this.dataForm.invalid) {
      this.toastrService.warning("Please complete the form before submitting", "Incomplete Form");
      return false;
    }

    if (this.mode == "NEW") {
      if (!this.verifyPatientType('save')) {
        return;
      }
      this.coolDialogs.confirm("Are you sure to register this " + this.staffPatient + " information?")
        .subscribe(res => {
          if (res) {
            let person = this.dataForm.value;
            let regPerson = { ic_no: person.ic_no, reg_no: this.dataRegistration.reg_no, person_type_code: "PATIENT", admin_type: "NONE", patient_type_code: person.age > 18 ? 'ADULT' : 'CHILD' };

            let data = {
              "person": person,
              "registration": regPerson
            };
            this.dataService.registerPerson(data).subscribe(reg => {
              this.toastrService.success(reg.message, "Success");
              this.refreshRegistrationInfo();
              this.modalRef.hide();
            }, msg1 => {
              this.toastrService.error(msg1.error.message, "Failed");
            });
          }
        });
    } else {
      this.coolDialogs.confirm("Are you sure to update this " + this.staffPatient + " information?")
        .subscribe(res => {
          if (res) {
            let person = this.dataForm.value;
            this.dataService.saveUpdatePerson(person).subscribe(reg => {
              this.toastrService.success(reg.message, "Success");
              this.refreshRegistrationInfo();
              this.modalRef.hide();
            }, msg1 => {
              this.toastrService.error(msg1.error.message, "Failed");
            });
          }
        });
    }
  }

  removeRegPerson(patient) {
    this.coolDialogs.confirm("Are you sure to remove details for " + patient.person.name + "?")
      .subscribe(res => {
        if (res) {
          this.dataService.removePatient(patient).subscribe(reg => {
            this.toastrService.success(reg.message, "Success");
            this.refreshRegistrationInfo();
          }, msg1 => {
            this.toastrService.error(msg1.error.message, "Failed");
          });
        }
      });
  }

  completeMyRegistration() {
    if (!this.verifyPatientType('complete')) {
      return;
    }

    if (this.dataRegistration.screening_plan.package_patients.length != this.dataPatients.length && !this.dataRegistration.company.co_reg_no) {
      this.toastrService.warning("Please complete all required information before submitting", "Data incomplete");
      return;
    }

    this.coolDialogs.confirm("I hereby confirm the details provided above and agree to the terms and conditions of the MyHealthCard application")
      .subscribe(confirm => {
        if (confirm) {
          let status = "PENDING-APPROVAL";
          let data = { "reg_no": this.dataRegistration.reg_no, "field_name": "status", "field_value": status }
          this.dataService.updateRegField(data).subscribe(res => {
            if (!res.errorFound) {
              this.dataRegistration.status = status;
              this.toastrService.success("Your application has been submitted and pending for approval", "Success");
            } else {
              this.toastrService.error("Unable to submit your applicaton", "Failed");
            }
          }, error => {
            this.toastrService.error("Unable to submit your applicaton", "Failed");
          });
        }
      });
  }

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  setDefaultPic(event) {
    event.target.src = "assets/images/avatar.jpg";
  }


}
