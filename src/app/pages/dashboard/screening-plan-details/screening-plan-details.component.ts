import { Input, TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../../service/data.service';
import { environment } from '../../../../environments/environment';

declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-dsh-screening-plan-details',
  templateUrl: './screening-plan-details.component.html',
  styleUrls: [
    './screening-plan-details.component.scss'
  ]
})
export class DashboardScreeningPlanDetailsComponent implements OnInit {
 
  modalRef: BsModalRef;
 
  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService
  ) { }

  @Input('data-registration') dataRegistration:any;
  user:any;
  // dataRegistration:any;
  auth:any;
  dataPatients:any;
  staffPatient:any;
  loading=true;
  photoPath :any;
  ngOnInit() {
    this.photoPath = environment.uploadPath + "package/";
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataService.getUserDetails(this.auth.username).subscribe(user=> {
      this.user = user;

      // this.dataService.getRegistrationInfo(this.user.registration.reg_no).subscribe(response=>{
      //   this.dataRegistration = response;
        if (this.dataRegistration.screening_plan.category_code=='CORPORATE') this.staffPatient = 'staff'; else this.staffPatient = 'patient';
        this.dataPatients = this.dataRegistration.registration_persons;
      //   console.log(response);
      //   this.loading = false;
      // }, error => {
      //   this.loading = false;
      // });
      this.loading = false;
    }, error=>{
      this.loading = false;
    });
  }

  setDefaultPic(event) {
    event.target.src = "assets/images/no_picture.png";
  }
 

}
