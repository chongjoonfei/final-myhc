import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { MauPageComponent } from './mau-page.component';
 
import { TooltipModule } from 'ngx-bootstrap';
import { MaterialModule } from '../../shared/material-module';
import { RouterModule } from '@angular/router';
 

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ChartModule,
    TooltipModule.forRoot(),
    MaterialModule,
    RouterModule,
  ],
  declarations: [
    MauPageComponent,
  ]
})
export class MauPageModule { }
