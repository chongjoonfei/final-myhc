import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';
import * as _ from "lodash";


@Component({
  selector: 'app-add-on-purchase',
  templateUrl: './add-on-purchase.component.html',
  styleUrls: [
    './add-on-purchase.component.scss'
  ]
})
export class AddOnPurchaseComponent implements OnInit {

  responsiveOptions: any;

  
  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

  mode: any;
  modalRef: BsModalRef;
  isLoading = true;
  loading = true;
  isSubmitted = false;
  dataPurchase:any;
  // dataRegistration:any;
  dataPatients:any;
  auth:any;
  user:any;

  planTestItems:any;
  planServices:any;
  dataRegistration:any;
  ngOnInit() {
    // this.fetchData();
 
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));
    this.planTestItems = this.dataRegistration.screening_plan.package_test_groups;
    this.planServices = this.dataRegistration.screening_plan.package_add_ons;

    // this.dataService.getUserDetails(this.auth.username).subscribe(res=> {
    //   this.user = res;
    //   this.planTestItems = res.registration.screening_plan.package_test_groups;
    //   this.planServices = res.registration.screening_plan.package_add_ons;
    //   // console.log(res.registration);
    //   this.loading=false;
    // }, error=>{
    //   this.loading=false;
    // });
    
    // this.dataService.getAddOnPurchaseHistory().subscribe(data => {
    //   this.dataPurchase = _.filter(data,{registration_no:this.user.registration_no});
    // });

    // this.fetchTestItem();
  }

  
  selectedPlan : any;
  dataFormAddOnPurchase: FormGroup;
  selectedAddOn: any;
  buyTestItem(template: TemplateRef<any>, data,addOnType) {
    // console.log(data);
    if (addOnType=='TEST'){
      this.selectedAddOn = {
        code: data.test_group.test_group_code,
        name: data.test_group.group_name,
        price: data.test_group.price,
        remark: data.remark,
        test_included:null
      }
    }else{
      this.selectedAddOn = {
        code: data.add_on.add_on_code,
        name: data.add_on.name,
        price: data.add_on.price,
        remark: data.add_on.remark
      }
    }
    // this.selectedPlan = data;
    this.mode = "BUY";
    // this.dataFormAddOnPurchase = this.formBuilder.group({
    //   "test_group_code": [data.test_group_code, Validators.required],
    //   "package_category": null,
    //   "group_name": [data.group_name, Validators.required],
    //   "patient_type": [data.patient_type],
    //   "price": [data.price, Validators.required],
    //   "enabled": [data.enabled]

    // });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-m' });
  }

  confirmPurchaseTestItem() {
    this.isSubmitted = true;
    this.coolDialogs.confirm("Are you sure to you would like to purchase this test item?")
      .subscribe(res => {
        if (res) {

          this.toastrService.info("Payment method is not implemented yet", "Not implemented");
 
            // this.dataService.createAddOnPackage(this.dataFormAddOnPurchase.value).subscribe(data => {
            //   if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
            //     this.toastrService.success(data.message, "Success");
            //     this.isSubmitted = false;
            //     this.fetchData();
            //     this.modalRef.hide();
            //     // this.mode == "EDIT";
            //   }
            // }, error => {
            //   if (error.status == 409)
            //     this.toastrService.error("Unable to purchase new test item - data already exist", "Warning");
            //   else
            //     this.toastrService.error("Unable to purchase new test item - service unavailabe", "Warning");
            // });
 
        }
      });
  }


// fetchData(){
//   this.user = JSON.parse(localStorage.getItem("user"));
//     this.dataService.getAddOnPurchaseHistory().subscribe(data => {
//       this.dataPurchase = _.filter(data,{registration_no:this.user.registration_no});
//     });
//   }

  // dataAddOnItem: any;
  // fetchTestItem() {
  //   this.dataService.getAllAddOnPackage().subscribe(res => {
  //     if (res.data) this.dataAddOnItem = res.data;
  //     console.log(res);
  //     this.isLoading = false;
  //   });

  // }



}

