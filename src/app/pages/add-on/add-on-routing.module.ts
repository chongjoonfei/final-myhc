import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddOnManageComponent } from './manage/add-on-manage.component';
import { AddOnPurchaseComponent } from './purchase/add-on-purchase.component';

const routes: Routes = [
  {
    path: 'purchase',
    component: AddOnPurchaseComponent,
  },
  {
    path: 'manage',
    component: AddOnManageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddOnRoutingModule { }
