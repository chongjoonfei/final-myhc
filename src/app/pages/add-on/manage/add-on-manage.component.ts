import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';


@Component({
  selector: 'app-add-on-manage',
  templateUrl: './add-on-manage.component.html',
  styleUrls: [
    './add-on-manage.component.scss'
  ]
})
export class AddOnManageComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) { }

  isLoading = true;
  modalRef: BsModalRef;
  dataAddOn: any;
  mode: any;
  dataNewAddOn: any;
  dataForm: FormGroup;
  dataFormAddOn: FormGroup;

  isSubmitted = false;

  ngOnInit() {
    this.fetchData();
    this.fetchPatientType();
    this.fetchRoleBased();
  }

  fetchData() {
    this.dataService.getAllAddOn().subscribe(result => {
      if (result.data) this.dataAddOn = result.data;
    }, error => {
      this.isLoading = false;
    });
  }

  iTemsPermission:any=[];
  iMainId:any=[];
  RoleBase:any=[];
  fetchRoleBased()
  {

    let auth = JSON.parse(localStorage.getItem("auth"));
     // Get the Menu Owner
    this.dataService.getMainMenu(auth.account.menu_owner).subscribe(response=> {
      //console.log(response.data);
      for (var key in response.data) {
        for(var key2 in response.data[key]['items'])
        {
          var uqk = response.data[key]['items'][key2]['path'] +'/'+response.data[key]['items'][key2]['page'];
          this.iTemsPermission[uqk] = response.data[key]['items'][key2]['item_id'];
          var mainid = response.data[key]['items'][key2]['item_id'];
          this.iMainId[mainid] = response.data[key]['main_id'];
         }
      }
      var ItemId = this.iTemsPermission['/add-on/manage'];
      var MainId = this.iMainId[ItemId];
      this.dataService.getMenuItemById(ItemId, MainId).subscribe(res => {
      this.RoleBase = res;
      console.log(res);
      });

    });

  }



  newAddOn(template: TemplateRef<any>) {
    this.mode = "NEW";

    // Role Based
    if(this.RoleBase.create==0)
    {
      this.translate.get('app').subscribe(data => {
        this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
      });
      return false;
    }

     this.dataFormAddOn = this.formBuilder.group({
      "uid": [0],
      "add_on_code": ['AOT'],
      "name": [null, Validators.required],
      "price": [null, Validators.required],
      "unit": null,
      "unit_decimal": 0,
      "remark": null,
      "status": [null, Validators.required],
      "no_of_patient": 1,
      "patient_type_code": null
        });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOn() {
    this.isSubmitted = true;
    if (this.dataFormAddOn.invalid) {
      this.translate.get('add-on-manage').subscribe(lang => {
        this.toastrService.warning(lang.notification.title_incompleteDetails, lang.notification.title_incompleted);
      });
      return;
    }

    this.translate.get('add-on-manage').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {

            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }

            this.dataService.ceateAddOn(this.dataFormAddOn.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
                this.toastrService.success(data.message, lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                console.log(data);

              // Auditrail
              let auth = JSON.parse(localStorage.getItem("auth"));

              let dataAut = {
                intProject :1,
                strModuleNamePathTblName :"Add-on >> manage >> add_on_services",
                intRecordId :0,
                intAction :3, // 1=Login,2=Logout,3=Create,4=Read,5=Update,6=Delete
                strAddedByUsername :auth.account.username,
                strAddedByEmail :auth.account.email
              };
              this.dataService.createAuditrail(dataAut).subscribe(resp=>{
                console.log(resp);
              },error =>{console.log(error);});
              }

            }, error => {
              if (error.status == 409)
                this.toastrService.error(lang.notification.title_dataExistAddOn, lang.notification.title_warning);
              else
                this.toastrService.error(lang.notification.title_serviceUnavailableAddOn, lang.notification.title_warning);
            });
          } else {

            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.updateAddOn(this.dataFormAddOn.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
                this.toastrService.success(data.message, lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error(lang.notification.title_failedUpdateAddOn, lang.notification.title_warning);
            });
          }
        }
      });
    });
  }

  editAddOn(template: TemplateRef<any>, data) {

    this.mode = "EDIT";
    this.dataFormAddOn = this.formBuilder.group({
      "uid": [data.uid],
      "add_on_code": [data.add_on_code],
      "name": [data.name, Validators.required],
      "price":[data.price, Validators.required],
      "unit": data.unit,
      "unit_decimal": data.unit_decimal,
      "remark": data.remark,
      "status": [data.status, Validators.required],
      "no_of_patient": data.no_of_patient,
      "patient_type_code": data.patient_type_code,
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  deleteRecordAddOn(code) {

    if(this.RoleBase.delete==0)
    {
      this.translate.get('app').subscribe(data => {
        this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
      });
      return false;
    }

    this.translate.get('add-on-manage').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_deleteAddOn)
      .subscribe(res => {
        if (res) {
          this.dataService.deleteAddOn(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warninig); else {
              this.toastrService.success(data.message, lang.notification.title_success);
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.notification.title_failedDeleteAddOn, lang.notification.title_warninig);
      });
    });
  }


  statusTypes = [
    { status: 'Enabled' },
    { status: 'Disabled' }
  ]

  patientTypes: any = [];
  fetchPatientType() {
    this.dataService.getLPatientType().subscribe(result => {
      if (result.data) this.patientTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

}

