import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChartModule} from 'angular2-chartjs';
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import {SharedModule} from '../../shared/shared.module';
import { MaterialModule } from '../../shared/material-module';
import { BookingRoutingModule } from './marketplace-routing.module';
import { MarketplaceComponent } from './marketplace.component';
import { MpBookingComponent } from './booking/mp-booking.component';
import { MpListingComponent } from './listing/mp-listing.component';
import { MpBookingCompletedComponent } from './booking/mp-booking-completed.component';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    BookingRoutingModule,
    SharedModule,
    ChartModule,
    QRCodeSVGModule,
    SlickCarouselModule,
    TooltipModule.forRoot(),
    MaterialModule,
    TranslateModule,
    HttpClientModule,
  ],
  declarations: [
    MpListingComponent,
    MpBookingComponent,
    MpBookingCompletedComponent,
    MarketplaceComponent,
    // ReceiptComponent
  ],
  exports: [
    TranslateModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class MarketplaceModule { }
