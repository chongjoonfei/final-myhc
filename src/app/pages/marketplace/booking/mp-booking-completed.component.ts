import { AfterViewInit, ChangeDetectorRef, Input, TemplateRef, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { MatStepper } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { DatePipe } from '@angular/common';

 

@Component({
  selector: 'app-mp-booking-completed',
  templateUrl: './mp-booking-completed.component.html',
  styleUrls: [
    './mp-booking.component.scss'
  ]
})
export class MpBookingCompletedComponent implements OnInit  {

  constructor(
    private modalService: BsModalService,
    private datePipe: DatePipe,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private router:Router
  ) { }

  @Input('data-registration') dataRegistration: any;

  user: any;
  dataBookingPayment :any;
  auth: any;
  dataPatients: any;
  loading = true;
  registrationPersons: any = [];
  totalPayment:any=0;
  ngOnInit() {
    let bookingNo = this.route.snapshot.paramMap.get('bookingno');

    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

    this.registrationPersons = _.filter(this.dataRegistration.registration_persons, { person_type_code: 'PATIENT' });

    this.auth = JSON.parse(localStorage.getItem("auth"));

    this.dataService.getBooking(bookingNo).subscribe(data=>{
      this.dataBookingPayment = data;
      this.dataBookingPayment.payments.forEach(element => {
        this.totalPayment+=element.amount;
      });
    });

    this.dataService.getUserDetails(this.auth.username).subscribe(user => {
      this.user = user;
    }, error => {
      this.loading = false;
    });
  }

  mode: any;

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  backToMarketplaceListing(){
    this.router.navigate(["marketplace","mp-listing"]);
  }

}
