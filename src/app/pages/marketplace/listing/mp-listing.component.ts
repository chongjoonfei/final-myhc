import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-mp-listing',
  templateUrl: './mp-listing.component.html',
  styleUrls: [
    './mp-listing.component.scss'
  ]
})
export class MpListingComponent implements OnInit {


  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  modalRef: BsModalRef;
  @Input('data-registration') dataRegistration:any;

  slides = [
    'https://via.placeholder.com/150x80',
    'https://via.placeholder.com/150x80',
    'https://via.placeholder.com/150x80',
    'https://via.placeholder.com/150x80',
    'https://via.placeholder.com/150x80',
    'https://via.placeholder.com/150x80',
    'https://via.placeholder.com/150x80',
    'https://via.placeholder.com/150x80'
  ];

  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    "infinite": false
  };

  user: any;
  // dataRegistration: any;
  auth: any;
  dataPatients: any;
  staffPatient: any;
  loading = true;
  planTestItems:any;
  planServices:any;
  planServicesDesc: any = [];
  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

    // console.log("dataRegistration",this.dataRegistration)
    this.planTestItems = this.dataRegistration.screening_plan.package_test_groups;
    this.planServices = this.dataRegistration.screening_plan.package_add_ons;

    this.planServices.forEach((srvs, i) => {
      this.planServicesDesc[i] = srvs.add_on.name + "<br />CODE: " + srvs.add_on.add_on_code;

    });


    console.log(this.planServicesDesc[2]);

    // console.log(this.planTestItems, this.planServices);

    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataService.getUserDetails(this.auth.username).subscribe(user => {
      this.user = user;
      this.fetchRegistrationInfo();
    }, error => {
      this.loading = false;
    });
    this.loadTexts();
  }

  texts={
    "patient-marketplace":"",
  }

  loadTexts(){
    this.dataService.getContent("patient-marketplace").toPromise().then((e)=>{
      console.log(e);
      this.texts[e.name] = e.content;
    })
  }

  addRegPersonNote:any;
  fetchRegistrationInfo() {
    this.addRegPersonNote=null;
    this.loading = true;
      if (this.dataRegistration.screening_plan.category_code == 'CORPORATE') this.staffPatient = 'staff'; else this.staffPatient = 'patient';
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'person_type_code': 'PATIENT' });
      this.dataRegistration.screening_plan.package_patients.forEach(element => {
        if (this.addRegPersonNote!=null)
          this.addRegPersonNote = this.addRegPersonNote + ' and ' + element.total_patient + " " + element.patient_type_code;
        else
          this.addRegPersonNote = element.total_patient + " " + element.patient_type_code;
      });
      // this.isSubmitted = false;
      this.loading = false;
      // console.log(this.dataRegistration);
  }

  refreshRegistrationInfo(){
    this.dataService.getRegistrationInfo(this.user.registration.reg_no).subscribe(res => {
      this.dataRegistration = res;
      this.fetchRegistrationInfo();
    });
  }


  book(type,item){
    console.log(item);
    if (type=="AOT")
      this.router.navigate(["marketplace","mp-booking",type,item.package_code,item.test_group_code]);
    else
      this.router.navigate(["marketplace","mp-booking",type,item.package_code,item.add_on_code]);
  }



  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  aotItem:any;
  openAotInfo(template: TemplateRef<any>,data){
    this.aotItem = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  aosItem:any;
  openAosInfo(template: TemplateRef<any>,data){
    this.aosItem = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

}
