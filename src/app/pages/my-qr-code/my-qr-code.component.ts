import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import * as _ from "lodash";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-my-qr-code',
  templateUrl: './my-qr-code.component.html',
  styleUrls: [
    './my-qr-code.component.scss'
  ]
})
export class MyQrCodeComponent implements OnInit {

  constructor(private dataService: DataService,public translate: TranslateService) {}

  user:any;
  auth:any;
  dataRegistration:any;
  qrActive:any;
  qrValue: any;
  qrName: any;
  dataPatients:any=[];
  errorCorrectionLevel='Q';
  buttonActive = 1;
  tabClass1: any;
  tabClass2: any;
  tabContent1 = false;
  tabContent2 = false;

  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    "infinite": false
  };


  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));

    // console.log(this.dataRegistration);


    if (this.dataRegistration.screening_plan.category_code == 'CORPORATE'
        || this.dataRegistration.screening_plan.category_code == 'INDIVIDUAL') {
      this.dataPatients = _.filter(this.dataRegistration.registration_persons, { 'ic_no': this.auth.account.ic_no });
    } else {
      this.dataPatients = this.dataRegistration.registration_persons;
    }

    this.qrActive = 0;
    this.qrName = this.dataPatients[0].person.name;
    this.qrValue = "Name:" + this.dataPatients[0].person.name + "; IC No:" + this.dataPatients[0].person.ic_no
    + "; Package:" + this.dataRegistration.screening_plan.package_code
    + "; Expired:" + this.dataRegistration.date_expired ;

    this.tabContent1 = true;
    this.tabClass1 = 'myhc-btn-active'

    // console.log(this.qrValue);

    // this.dataService.getUserDetails(this.auth.username).subscribe(user=> {
    //   this.user = user;
    //   this.qrValue = "Name:" + this.user.person.name + "; IC No:" + this.user.person.ic_no
    //   + "; Package:" + this.dataRegistration.screening_plan.package_code
    //   + "; Expired:" + this.dataRegistration.date_expired ;
    // });
  }

  tabClick(evt){
    this.qrValue = "Name:" + this.dataPatients[evt.index].person.name + "; IC No:" + this.dataPatients[evt.index].person.ic_no
    + "; Package:" + this.dataRegistration.screening_plan.package_code
    + "; Expired:" + this.dataRegistration.date_expired ;
  }

  tab(evt) {
    if (evt == 1) {
      this.tabClass1 = 'myhc-btn-active';
      this.tabClass2 = '';
      this.tabContent1 = true;
      this.tabContent2 = false;
    } else {
      this.tabClass1 = '';
      this.tabClass2 = 'myhc-btn-active';
      this.tabContent1 = false;
      this.tabContent2 = true;
    }
  }

  showProfile(index) {
    this.qrActive = index;
    this.qrName = this.dataPatients[index].person.name;

    this.qrValue = "Name:" + this.dataPatients[index].person.name + "; IC No:" + this.dataPatients[index].person.ic_no
    + "; Package:" + this.dataRegistration.screening_plan.package_code
    + "; Expired:" + this.dataRegistration.date_expired ;
  }


}

 