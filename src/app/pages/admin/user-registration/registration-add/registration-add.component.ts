import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DataService } from '../../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { environment } from '../../../../../environments/environment';
import * as _ from "lodash";



@Component({
  selector: 'app-admin-registration-add',
  templateUrl: './registration-add.component.html',
  styleUrls: ['./registration-add.component.scss']
})
export class RegistrationAddComponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private router: Router,
    public translate: TranslateService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {

  }

  loading = true;

  ngOnInit() {
    this.fetchRoleBased();
    this.newRegistration();
    this.fetchLookupInfo();
  }

  iTemsPermission:any=[];
  iMainId:any=[];
  RoleBase:any=[];
  fetchRoleBased()
  {

    let auth = JSON.parse(localStorage.getItem("auth"));
     // Get the Menu Owner
    this.dataService.getMainMenu(auth.account.menu_owner).subscribe(response=> {
      //console.log(response.data);
      for (var key in response.data) {
        for(var key2 in response.data[key]['items'])
        {
          var uqk = response.data[key]['items'][key2]['path'] +'/'+response.data[key]['items'][key2]['page'];
          this.iTemsPermission[uqk] = response.data[key]['items'][key2]['item_id'];
          var mainid = response.data[key]['items'][key2]['item_id'];
          this.iMainId[mainid] = response.data[key]['main_id'];
         }
      }
      var ItemId = this.iTemsPermission['/administrator/user-registration'];
      var MainId = this.iMainId[ItemId];
      this.dataService.getMenuItemById(ItemId, MainId).subscribe(res => {
      this.RoleBase = res;
      });
    });
  }

  optChange(evt){
    this.formDetails.get("package_code").setValue(evt.target.value);
  }

  objectKeys = Object.keys;
  screeningPlans: any = [];
  newRegistration() {
    this.isSubmitted = false;
    this.formDetails = this.formBuilder.group({
      name: [null, Validators.required],
      // password: null,
      ic_no: [null, Validators.required],
      email: [null, Validators.required],
      mobile_no: [null, Validators.required],
      package_code: [null, Validators.required],
      amount_paid: [null, Validators.required],
      payment_no: [null, Validators.required],
      payment_date: [null, Validators.required],
      payment_method: 'CASH',
      date_registered: null,
      date_expired: null,
      status: 'PRE-ACCOUNT',
      center_code: 'MONT KIARA'
    });
    this.screeningPlans = [];
    this.dataService.getAllScreeningPlan().subscribe(res => {
      let screeningPlans = _.groupBy(res.data, 'category_code');
      for (var key in screeningPlans) {
        this.screeningPlans.push({ 'category': key, 'plans': screeningPlans[key] })
      }
    });
  }

  formDetails: FormGroup;
  isSubmitted=false;
  optPackageCode:any;
  registerNewUser() {

    this.isSubmitted = true;
    if (this.formDetails.invalid){
      this.translate.get('user-registration').subscribe(data => {
        this.toastrService.warning(data.error.title_incomplete_desc, data.error.title_incomplete);
      });
      return;
    }
    this.translate.get('user-registration').subscribe(data => {

      this.coolDialogs.confirm(data.notification.title_confirmation)
      .subscribe(res => {
        if (res) {

          // Role Based
          if(this.RoleBase.create==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });

            return false;
          }

          // Auditrail
          let auth = JSON.parse(localStorage.getItem("auth"));

          let dataAut = {
            intProject :1,
            strModuleNamePathTblName :"User-registration >> person",
            intRecordId :0,
            intAction :3, // 1=Login,2=Logout,3=Create,4=Read,5=Update,6=Delete
            strAddedByUsername :auth.account.username,
            strAddedByEmail :auth.account.email
          };
          this.dataService.createAuditrail(dataAut).subscribe(resp=>{
            console.log(resp);
          },error =>{console.log(error);});

          this.dataService.preRegister(this.formDetails.value).subscribe(result => {
            this.router.navigate(['/administrator/user-registration']);
            this.toastrService.success(data.notification.title_successRegister_desc, data.notification.title_success);
          }, err => {
            this.toastrService.error(err.error.message, data.error.title_processFailed);
          });
          this.isSubmitted = false;
        }
      });
    });

  }

  states: any;
  paymentMethods: any;
  fetchLookupInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
    this.dataService.getPaymentMethods().subscribe(res => {
      this.paymentMethods = res.data;
    });

  }
}
