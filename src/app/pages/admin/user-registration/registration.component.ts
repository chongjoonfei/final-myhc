import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
import * as _ from "lodash";
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';


@Component({
  selector: 'app-admin-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {

  }

  loading = true;
  modalRef: BsModalRef;
  dataRegistration: any;
  ngOnInit() {
    this.fetchRoleBased();
    this.fetchRegistrations();
    this.fetchLookupInfo();
  }

  keyword:any;
  dataPerson:any;
  isSearching=false;
  searchPerson() {

    this.dataPerson = null;
    this.isSearching = true;
    this.dataService.searchPerson(this.keyword).subscribe(res => {
      this.dataPerson = res.data;
      //console.log("search",res.data);
      this.isSearching = false;
    }, error=>{
      this.isSearching = false;
    });
  }

  fetchRegistrations() {
    this.loading=true;
    this.dataService.getAllRegistration().subscribe(res => {
      this.dataRegistration = res.data;
      this.loading = false;
    }, error=>{
      this.loading = false;
    });
  }

  iTemsPermission:any=[];
  iMainId:any=[];
  RoleBase:any=[];
  fetchRoleBased()
  {
    let auth = JSON.parse(localStorage.getItem("auth"));
     // Get the Menu Owner
    this.dataService.getMainMenu(auth.account.menu_owner).subscribe(response=> {
      //console.log(response.data);
      for (var key in response.data) {
        for(var key2 in response.data[key]['items'])
        {
          var uqk = response.data[key]['items'][key2]['path'] +'/'+response.data[key]['items'][key2]['page'];
          this.iTemsPermission[uqk] = response.data[key]['items'][key2]['item_id'];
          var mainid = response.data[key]['items'][key2]['item_id'];
          this.iMainId[mainid] = response.data[key]['main_id'];
         }
      }
      // Based on the segment and page
      var ItemId = this.iTemsPermission['/administrator/user-registration'];
      var MainId = this.iMainId[ItemId];
      this.dataService.getMenuItemById(ItemId, MainId).subscribe(res => {
      this.RoleBase = res;
      });
    });
  }

  objectKeys = Object.keys;
  screeningPlans: any = [];
  newRegistration(template: TemplateRef<any>) {

    this.isSubmitted = false;
    this.formDetails = this.formBuilder.group({
      name: [null, Validators.required],
      // password: null,
      ic_no: [null, Validators.required],
      email: [null, Validators.required],
      mobile_no: [null, Validators.required],
      package_code: [null, Validators.required],
      amount_paid: [null, Validators.required],
      payment_no: [null, Validators.required],
      payment_date: [null, Validators.required],
      payment_method: 'CASH',
      date_registered: null,
      date_expired: null,
      status: 'PRE-ACCOUNT',
      center_code: 'MONT KIARA'
    });
    this.screeningPlans = [];
    this.dataService.getAllScreeningPlan().subscribe(res => {
      let screeningPlans = _.groupBy(res.data, 'category_code');
      for (var key in screeningPlans) {
        this.screeningPlans.push({ 'category': key, 'plans': screeningPlans[key] })
      }
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  formDetails: FormGroup;
  isSubmitted=false;
  optPackageCode:any;
  registerNewUser() {
    this.isSubmitted = true;
    if (this.formDetails.invalid){
      this.toastrService.warning("Please complete the form details before submitting", "Incomplete information");
      return;
    }
    this.coolDialogs.confirm("Are you sure to register this user?")
      .subscribe(res => {
        if (res) {

          if(this.RoleBase.create==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }

          // console.log(this.formDetails.value);
          this.dataService.preRegister(this.formDetails.value).subscribe(result => {
            this.fetchRegistrations();
            this.toastrService.success("The user PRE-ACCOUNT has been successfully created.", "Success");
            this.modalRef.hide();
          }, err => {
            this.toastrService.error(err.error.message, "Process failed");
          });
          this.isSubmitted = false;
        }
      });
  }

  optChange(evt){
    this.formDetails.get("package_code").setValue(evt.target.value);
  }

  dataForm: FormGroup;
  dataUserReg: any;
  manageRegistration(template: TemplateRef<any>, data) {
    this.fetchLookupInfo();
    this.dataUserReg = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }


  docUrl: any;
  printDoc(url) {
    this.docUrl = environment.uploadPath + url;
    var proxyIframe = document.createElement('iframe');
    var body = document.getElementsByTagName('body')[0];
    body.appendChild(proxyIframe);
    proxyIframe.style.width = '100%';
    proxyIframe.style.height = '100%';
    proxyIframe.style.display = 'none';

    var contentWindow = proxyIframe.contentWindow;
    contentWindow.document.open();
    // Set dimensions according to your needs.
    // You may need to calculate the dynamically after the content has loaded
    contentWindow.document.write('<iframe src="' + this.docUrl + '" onload="print();" width="1000" height="1800" frameborder="0" marginheight="0" marginwidth="0">');
    contentWindow.document.close();
  }

  deleteApplication() {
    this.coolDialogs.confirm("Are you sure to delete this application?")
      .subscribe(confirm => {
        if (confirm) {
          this.dataService.deleteApplication(this.dataUserReg).subscribe(res => {
            if (!res.errorFound) {
              this.dataUserReg.status = status;
              this.toastrService.success("This application has been deleted", "Success");
              this.fetchRegistrations();
              this.modalRef.hide();
            } else {
              this.toastrService.error("Unable to delete your applicaton", "Failed");
            }
          }, error => {
            this.toastrService.error("Unable to delete this applicaton", "Failed");
          });
        }
      });
  }

  blockApplication() {
    this.coolDialogs.confirm("Are you sure to block this application?")
      .subscribe(confirm => {
        if (confirm) {
          let status = "BLOCKED";
          let data = { "reg_no": this.dataUserReg.reg_no, "field_name": "status", "field_value": status }
          this.dataService.updateRegField(data).subscribe(res => {
            if (!res.errorFound) {
              this.dataUserReg.status = status;
              this.fetchRegistrations();
              this.toastrService.success("The application has been blocked", "Success");
            } else {
              this.toastrService.error("Unable to block this applicaton", "Failed");
            }
          }, error => {
            this.toastrService.error("Unable to block this applicaton", "Failed");
          });
        }
      });
  }

  approveApplication() {
    if (!this.verifyPatientType()){
      return;
    }

    this.coolDialogs.confirm("Are you sure to approve this application?")
      .subscribe(confirm => {
        if (confirm) {
          let status = "APPROVED";
          let data = { "reg_no": this.dataUserReg.reg_no, "field_name": "status", "field_value": status };
          this.dataService.updateRegField(data).subscribe(res => {
            if (!res.errorFound) {
              this.dataUserReg.status = status;
              this.fetchRegistrations();
              this.toastrService.success("This application has been approved", "Success");
            } else {
              this.toastrService.error("Unable to approve this applicaton", "Failed");
            }
          }, error => {
            this.toastrService.error("Unable to approve this applicaton", "Failed");
          });
        }
      });

  }

  verifyPatientType(){

    let dataPatients = _.filter(this.dataUserReg.registration_persons, { 'person_type_code': 'PATIENT' });

    // console.log("dataPatients",dataPatients);

    let isValid = true;
    let msg = "";

    let cPatient = _.countBy(dataPatients,'patient_type_code');


    Object.entries(cPatient).forEach(([key, value]) => {
       let comp = _.filter(this.dataUserReg.screening_plan.package_patients,{patient_type_code:key});

       if (comp.length>0){
        //  console.log(value + " > " + comp[0].total_patient);
         if (value > comp[0].total_patient){
          isValid = false;
          msg += "\nTotal " + key + " should not be more than " + comp[0].total_patient + " person(s)";
          return;
         }
       }
    });
    if (!isValid) this.toastrService.warning(msg,"Warning");
    return isValid;
  }

  resetApplication() {
    this.coolDialogs.confirm("Are you sure to reset status for this application to PRE-ACCOUNT?")
      .subscribe(confirm => {
        if (confirm) {
          let status = "PRE-ACCOUNT";
          let data = { "reg_no": this.dataUserReg.reg_no, "field_name": "status", "field_value": status }
          this.dataService.updateRegField(data).subscribe(res => {
            if (!res.errorFound) {
              this.dataUserReg.status = status;
              this.toastrService.success("Application status has been reset", "Success");
            } else {
              this.toastrService.error("Unable to reset applicaton status", "Failed");
            }
          }, error => {
            this.toastrService.error("Unable to reset applicaton status", "Failed");
          });
        }
      });
  }


  mode:any="VIEW";
  editUser(data) {
    this.mode = "EDIT";
    this.dataForm = this.formBuilder.group({
      name: [data.name, Validators.required],
      age: [data.age, Validators.required],
      ic_no: [data.ic_no, Validators.required],
      mobile_no: [data.mobile_no, Validators.required],
      email: [data.email, Validators.required],
      gender: [data.gender, Validators.required],
      patient_type_code: [data.patient_type_code],
      address: [data.address],
      town: [data.town],
      district: [data.district],
      postcode: [data.postcode],
      state: [data.state],
      status: [data.status],
      reg_no: this.dataRegistration.reg_no
    });
  }

  states: any;
  paymentMethods: any;
  fetchLookupInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
    this.dataService.getPaymentMethods().subscribe(res => {
      this.paymentMethods = res.data;
    });

  }
}
