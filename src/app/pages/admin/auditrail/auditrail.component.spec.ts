import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditrailComponent } from './auditrail.component';

describe('AuditrailComponent', () => {
  let component: AuditrailComponent;
  let fixture: ComponentFixture<AuditrailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditrailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditrailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
