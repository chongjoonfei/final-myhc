import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import * as _ from "lodash";
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-screening-plan',
  templateUrl: './screening-plan.component.html',
  styleUrls: ['./screening-plan.component.scss']
})
export class ScreeningPlanComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) { }


  mode: any;
  modalRef: BsModalRef;
  isLoading = true;
  isSubmitted = false;

  categoryCodeList = [
    { category_code: 'FAMILY' },
    { category_code: 'CORPORATE' },
    { category_code: 'INDIVIDUAL' },
    { category_code: 'DEPENDENT' }
  ]

  photoPath :any;
  ngOnInit() {
    this.photoPath = environment.uploadPath + "package/";
    this.fetchRoleBased();
    this.fetchData();
    this.fetchPatientType();
    this.fetchAddOn();
    this.fetchTestPanel();
    this.fetchLocation();
    this.fetchTestGroup();
  }
  iTemsPermission:any=[];
  iMainId:any=[];
  RoleBase:any=[];
  datapath:any;
  fetchRoleBased()
  {

    let auth = JSON.parse(localStorage.getItem("auth"));
     // Get the Menu Owner
    this.dataService.getMainMenu(auth.account.menu_owner).subscribe(response=> {
      //console.log(response.data);
      for (var key in response.data) {
        for(var key2 in response.data[key]['items'])
        {
          var uqk = response.data[key]['items'][key2]['path'] +'/'+response.data[key]['items'][key2]['page'];
          this.iTemsPermission[uqk] = response.data[key]['items'][key2]['item_id'];
          var mainid = response.data[key]['items'][key2]['item_id'];
          this.iMainId[mainid] = response.data[key]['main_id'];
         }
      }
      var ItemId = this.iTemsPermission['/administrator/screening-plan'];
      var MainId = this.iMainId[ItemId];
      this.dataService.getMenuItemById(ItemId, MainId).subscribe(res => {
      this.RoleBase = res;
      console.log(res);
      });

    });

  }

  fetchData() {
    this.dataService.getAllScreeningPlan().subscribe(result => {
      if (result.data) this.dataSPlan = result.data;
    }, error => {
      this.isLoading = false;
    });


  }
  selectedPackagePlan: any;
  // SCREENING PLAN
  dataSPlan: any;
  dataFormSPlan: FormGroup;

  newScreeningPlan(template: TemplateRef<any>) {
    this.isSubmitted = false;
    this.mode = "NEW";
    this.dataFormSPlan = this.formBuilder.group({
      "package_code": [null, Validators.required],
      "single_package": [null, Validators.required],
      "category_code": [null, Validators.required],
      "description": [null, Validators.required],
      "picture_path": [null],
      "price": [null, Validators.required],
      "license_validity_year": [null],
      "test_included": [null, Validators.required],
      "note": [null],
      "commercial": ["YES", Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveScreenPlan() {
    // console.log(this.dataFormSPlan.value);
    this.isSubmitted = true;
    if (this.dataFormSPlan.invalid) {

      this.translate.get('screening-plan').subscribe(lang => {
        this.toastrService.warning(lang.notification.title_incompleteDetails , lang.notification.title_warning);
      });
      return;
    }

    // console.log(this.mode);
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_deleteRecord + "?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.createScreenPlan(this.dataFormSPlan.value).subscribe(data => {
              if (data.errorFound)
                this.translate.get('screening-plan').subscribe(lang => {
                  this.toastrService.warning(data.message , lang.notification.title_warning);
                });


              else {
                this.translate.get('screening-plan').subscribe(lang => {
                  this.toastrService.warning(data.message , lang.notification.title_success);
                });


                this.fetchData();
                // this.modalRef.hide();
                this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.translate.get('screening-plan').subscribe(lang => {
                  this.toastrService.error(lang.notification.title_dataExist, lang.notification.title_warning);
                });
              else
                this.translate.get('screening-plan').subscribe(lang => {
                  this.toastrService.error(lang.notification.title_serviceUnavailable, lang.notification.title_warning);
                });

            });
          } else {
            if(this.RoleBase.update==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          this.translate.get('screening-plan').subscribe(lang => {
            this.dataService.updateScreenPlan(this.dataFormSPlan.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
                this.toastrService.success(data.message, lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                // this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error(lang.notification.title_failedUpdateSceeningPlan, lang.notification.title_warning);
            });
          });
          }
        }
      });
    });
  }

  editScreenPlan(template: TemplateRef<any>, data) {
    this.isSubmitted = false;
    this.selectedPackagePlan = data;
    this.mode = "EDIT";
    this.dataFormSPlan = this.formBuilder.group({
      "package_code": [data.package_code, Validators.required],
      "single_package": [data.single_package, Validators.required],
      "category_code": [data.category_code, Validators.required],
      "description": [data.description, Validators.required],
      "picture_path": [data.picture_path, Validators.required],
      "price": [data.price, Validators.required],
      "license_validity_year": [data.license_validity_year, Validators.required],
      "test_included": [data.test_included, Validators.required],
      "note": [data.note, Validators.required],
      "commercial": [data.commercial, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  deleteScreenPlan(code) {
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_deleteRecord + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          this.dataService.deleteScreenPlan(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
              this.toastrService.success(data.message, lang.notification.title_success);
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.notification.title_failedDeleteRecord, lang.notification.title_warning);
      });
    });
  }

  uploading = false;
  photoFile: any[] = [];
  tmpPhotoFile: any = [];
  uploadPhotoNow(event, pPhoto) {
    // console.log("uploadPhotoNow", event, pPhoto);
    this.photoFile = [];
    for (let file of event.files) {
      this.photoFile.push(file);
    }
    let packagePicture = null;
    // console.log("upload", this.uploadedFiles);
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_upload + this.photoFile[0].name + "?")
      .subscribe(res => {
        if (res) {
          this.uploading = true;
          var formData = new FormData();
          formData.append('package_code', this.selectedPackagePlan.package_code);
          for (var i = 0; i < this.photoFile.length; i++) {
            formData.append("fileToUpload[]", this.photoFile[i]);
            packagePicture = this.photoFile[i];
          }
          this.dataService.uploadPackagePicture(formData).subscribe(res => {
            if (res.errorFound) {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.toastrService.error(res.message, lang.notification.title_failed);
            } else {
              this.tmpPhotoFile = [];
              this.photoFile = [];
              this.dataFormSPlan.get('picture_path').setValue(this.selectedPackagePlan.package_code+"_"+packagePicture.name);
              this.selectedPackagePlan.picture_path = this.selectedPackagePlan.package_code+"_"+packagePicture.name;
              // console.log(packagePicture);
              this.toastrService.success(lang.notification.title_successUploadPhoto, lang.notification.title_success);
              // this.fetchPerson();
            }
            this.uploading = false;
          }, error => {
            this.toastrService.error(error.error.message, lang.notification.title_failed);
            this.uploading = false;
          });
        }
      });
    });
  }


  // PACKAGE PATIENT
  dataFormPP: FormGroup;
  dataPP: any;
  modePatient: any;

  editPackagePatient(patient) {
    console.log(patient)
    this.translate.get('screening-plan').subscribe(lang => {
      this.modePatient = lang.form.title_edit;
    });
    this.dataFormPP = this.formBuilder.group(patient);
  }

  newPackagePatient(package_code) {
    this.translate.get('screening-plan').subscribe(lang => {
      this.modePatient = lang.form.title_new;
    });
    this.dataFormPP = this.formBuilder.group({
      "package_code": [package_code, Validators.required],
      "patient_type_code": [null, Validators.required],
      "total_patient": [null, Validators.required],
      "doc_required": [null]
    });
  }

  deletePackagePatient(code) {
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_deleteRecord + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          this.dataService.deletePackagePatient(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
              this.toastrService.success(data.message, lang.notification.title_success);
              this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
                this.selectedPackagePlan = data;
              });
              this.modePatient = null;
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.notification.title_warning, lang.notification.title_warning);
      });
    });
  }

  savePackagePatient() {
    this.isSubmitted = true;
    if (this.dataFormPP.invalid) {
      this.translate.get('screening-plan').subscribe(lang => {
        this.toastrService.warning(lang.notification.title_incompleteDetails, lang.notification.title_warning);
      });
      return;
    }

    // console.log(this.mode);
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_saveInfo)
      .subscribe(res => {
        if (res) {
          if (this.modePatient == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.createSPPackagePatient(this.dataFormPP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
                this.toastrService.success(data.message, lang.notification.title_success);
                this.isSubmitted = false;
                // this.fetchData();
                this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
                  this.selectedPackagePlan = data;
                });
                this.modePatient = null;
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error( lang.notification.title_dataExistPateint,  lang.notification.title_warning);
              else
                this.toastrService.error( lang.notification.title_serviceUnavailablePatient,  lang.notification.title_warning);
            });
          } else {
            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.updateSPPackagePatient(this.dataFormPP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
                this.toastrService.success(data.message, lang.notification.title_success);
                this.isSubmitted = false;
                this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
                  this.selectedPackagePlan = data;
                });
                this.modePatient = null;
              }
            }, error => {
              this.toastrService.error(lang.notification.title_failedUpdatePatient, lang.notification.title_warning);
            });
          }
        }
      });
    });
  }

  // PACKAGE TEST PANEL

  dataFormPTP: FormGroup;
  dataPTP: any;

  newPackageTestPanel(template: TemplateRef<any>, package_code) {
    this.translate.get('screening-plan').subscribe(lang => {
      this.mode = lang.form.title_new;
    });
    this.dataFormPTP = this.formBuilder.group({
      "package_code": [package_code, Validators.required],
      "test_panel_code": [null, Validators.required],
      "test_location": [null, Validators.required],
      // "total_test_conducted": [null, Validators.required],
      "total_test_conducted":1,
      "remark": [null, Validators.required]

    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  savePackageTestPanel() {
    this.isSubmitted = true;
    if (this.dataFormPTP.invalid) {
      this.translate.get('screening-plan').subscribe(lang => {
      this.toastrService.warning(lang.notification.title_incompleteDetails, lang.notification.title_warning);
      });
      return;
    }

    // console.log(this.mode);
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_saveInfo)
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.createSPPacTestPanel(this.dataFormPTP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.notification.title_warning); else {
                this.toastrService.success(data.message, lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error(lang.notification.title_dataExistPateint, lang.notification.title_warning);
              else
                this.toastrService.error(lang.notification.title_serviceUnavailablePatient, lang.notification.title_warning);
            });
          } else {
            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.updateSPPacTestPanel(this.dataFormPTP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
                this.toastrService.success(data.message,  lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error(lang.notification.title_failedUpdatePatient,  lang.notification.title_warning);
            });
          }
        }
      });
    });
  }

  deleteSPPackageTestPanel(code) {
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_deleteRecord)
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
          this.dataService.deletePackageTestPanel(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
              this.toastrService.success(data.message,  lang.notification.title_success);
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.notification.title_failedDeleteRecord,  lang.notification.title_warning);
      });
    });
  }

 // PACKAGE ADD ON / PACKAGES
  dataFormAOPackage: FormGroup;
  dataAOPackage: any;
  newAddOnPackage(template: TemplateRef<any>, plan) {
    this.selectedPackagePlan = plan;
    this.translate.get('screening-plan').subscribe(lang => {
      this.mode = lang.form.title_new;
    });
    this.dataFormAOPackage = this.formBuilder.group({
      "package_code": [plan.package_code, Validators.required],
      "test_group_code": [null, Validators.required],
      "test_location": [null, Validators.required],
      // "total_test_conducted": [null, Validators.required],
      "total_test_conducted":1,
      "remark": [null, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOnPackage() {
    this.isSubmitted = true;
    if (this.dataFormAOPackage.invalid) {
      this.translate.get('screening-plan').subscribe(lang => {
        this.toastrService.warning(lang.notification.title_incompleteDetails,  lang.notification.title_warning);
      });
      return;
    }

    // console.log(this.mode);
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.createSPAddOnPackage(this.dataFormAOPackage.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
                this.toastrService.success(data.message,  lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error(lang.notification.title_dataExistPackage,  lang.notification.title_warning);
              else
                this.toastrService.error(lang.notification.title_serviceUnavailablePackage,  lang.notification.title_warning);
            });
          } else {
            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.updateSPAddOnPackage(this.dataFormAOPackage.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
                this.toastrService.success(data.message,  lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error(lang.notification.title_failedUpdatePatient,  lang.notification.title_warning);
            });
          }
        }
      });
    });
  }

  deleteAddOnPackage(code) {
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
          this.dataService.deleteSPAddOnPackage(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
              this.toastrService.success(data.message,  lang.notification.title_success);
              this.fetchData();
              // this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
              //   this.selectedPackagePlan = data;
              // });
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.notification.title_failedDeletePackage,  lang.notification.title_warning);
      });
    });
  }

  editAddOnPackage(addOnP) {
    this.translate.get('screening-plan').subscribe(lang => {
      this.modePatient = lang.form.title_edit;
    });
    this.dataFormPP = this.formBuilder.group(addOnP);
  }


  // PACKAGE ADD ON / SERVICES
  dataFormAOService: FormGroup;
  dataAOService: any;

  newAddOnService(template: TemplateRef<any>, plan) {
    console.log(plan);
    this.selectedPackagePlan = plan;
    this.translate.get('screening-plan').subscribe(lang => {
      this.mode = lang.form.title_new;
    });
    this.dataFormAOService = this.formBuilder.group({
      "package_code": [plan.package_code, Validators.required],
      "add_on_code": [null, Validators.required],
      "add_on_name": [null],
      "test_location_code": [null, Validators.required],
      "test_location_name": [null],
      // "total_test_conducted": [null, Validators.required],
      "total_test_conducted":1,
      "patient_type_code": [null, Validators.required]

    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOnService() {
    this.isSubmitted = true;
    if (this.dataFormAOService.invalid) {
      this.translate.get('screening-plan').subscribe(lang => {
        this.toastrService.warning( lang.notification.title_incompleteDetailsg,  lang.notification.title_warning);
      });
      return;
    }

    // console.log(this.mode);
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_deleteRecord + "?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.createSPAddOnService(this.dataFormAOService.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
                this.toastrService.success(data.message,  lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error(lang.notification.title_dataExistPackage,  lang.notification.title_warning);
              else
                this.toastrService.error(lang.notification.title_serviceUnavailablePackage,  lang.notification.title_warning);
            });
          } else {
            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.updateSPAddOnService(this.dataFormAOService.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
                this.toastrService.success(data.message,  lang.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error(lang.notification.title_failedUpdatePatient,  lang.notification.title_warning);
            });
          }
        }
      });
    });
  }

  deleteAddOnService(code) {
    this.translate.get('screening-plan').subscribe(lang => {
    this.coolDialogs.confirm(lang.notification.title_deleteRecord)
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
          this.dataService.deleteSPAddOnService(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message,  lang.notification.title_warning); else {
              this.toastrService.success(data.message,  lang.notification.title_success);
              this.fetchData();
              // this.dataService.getOneScreenPlan(this.selectedPackagePlan.package_code).subscribe(data => {
              //   this.selectedPackagePlan = data;
              // });
            }
          });
        }
      }, error => {
        this.toastrService.error( lang.notification.title_failedDeleteScreeningRecord,  lang.notification.title_warning);
      });
    });
  }

  editAddOnService(addOnS) {
    this.translate.get('screening-plan').subscribe(lang => {
      this.modePatient = lang.form.title_edit;
    });
    this.dataFormPP = this.formBuilder.group(addOnS);
  }



  // LOOKUP LIST
  patientTypes: any = [];
  fetchPatientType() {
    this.dataService.getLPatientType().subscribe(result => {
      if (result.data) this.patientTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  addOnTypes: any = [];
  fetchAddOn() {
    this.dataService.getAllAddOn().subscribe(result => {
      if (result.data) this.addOnTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  testPanelTypes: any = [];
  fetchTestPanel() {
    this.dataService.getAllTestPanel().subscribe(result => {
      if (result.data) this.testPanelTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }


  locationTypes: any = [];
  fetchLocation() {
    this.dataService.getLLocationType().subscribe(result => {
      if (result.data) this.locationTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  testGroupTypes: any = [];
  fetchTestGroup() {
    this.dataService.getAllAddOnPackage().subscribe(result => {
      if (result.data) this.testGroupTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  closeScreeningPlan() {
    this.modePatient = null;
    this.modalRef.hide();
    this.fetchData();
  }

  cancelPackagePatient() {
    this.modePatient = null;
    this.fetchData();

  }

  setDefaultPic(event) {
    event.target.src = "assets/images/no_picture.png";
  }

}
