import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-message-box2',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss']
})
export class MessageBox2Component implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {

  }

  isLoading = true;
  modalRef: BsModalRef;
  dataMessages: any;
  ngOnInit() {
    this.dataService.getMessages().subscribe(data => {
      this.dataMessages = data;
      console.log(data);
    });
  }

  selectedMessage: any;
  showMessages(template: TemplateRef<any>, data) {
    this.selectedMessage = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }





}
