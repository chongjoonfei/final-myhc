import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { AuthGuard } from 'src/app/service/auth/auth.guard';

import { MessageBox2Component } from './message-box2/message-box2.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard.component';
import { ScreeningPlanComponent } from './screening-plan/screening-plan.component';
import { RegistrationComponent } from './user-registration/registration.component';
import { RegistrationAddComponent } from './user-registration/registration-add/registration-add.component';
import { AdminSettingsomponent } from './admin-settings/admin-settings.component';
import { BookingMgmtComponent } from './booking-mgmt/booking-mgmt.component';
import { TransactionMgmtComponent } from './transaction-mgmt/transaction-mgmt.component';
import { AuditrailComponent } from './auditrail/auditrail.component';

import { ScreeningTestComponent } from './screening-test/screening-test.component';

const routes: Routes = [

      {
        path: '',
        component: AdminDashboardComponent,pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: AdminDashboardComponent,pathMatch: 'full'
      },
      {
        path: 'screening-plan',
        component: ScreeningPlanComponent,pathMatch: 'full'
      },
      {
        path: 'message-box',
        component: MessageBox2Component,pathMatch: 'full'
      },
      {
        path: 'user-registration',
        component: RegistrationComponent,pathMatch: 'full'
      },
      {
        path: 'user-registration/add',
        component: RegistrationAddComponent,pathMatch: 'full'
      },
      {
        path: 'admin-settings',
        component: AdminSettingsomponent,pathMatch: 'full'
      },
      {
        path: 'booking-mgmt',
        component: BookingMgmtComponent,pathMatch: 'full'
      },
      {
        path: 'transaction-mgmt',
        component: TransactionMgmtComponent,pathMatch: 'full'
      },
      {
        path: 'auditrail',
        component: AuditrailComponent,pathMatch: 'full'
      },
      {
        path: 'screening-test',
        component: ScreeningTestComponent,pathMatch: 'full'
      },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
