import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { DataService } from '../../../service/data.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import {TranslateService} from '@ngx-translate/core';

declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: [
    './admin-dashboard.component.scss'
  ]
})
export class AdminDashboardComponent implements OnInit {

  modalRef: BsModalRef;
  dataBookings:any;
  dataRegistration:any;

  singleGender = [
    {
      "name": "Male",
      "value": 19
    },
    {
      "name": "Female",
      "value": 12
    }
  ];

  singleAge = [
    {
      "name": "<30 yr old",
      "value": 7
    },
    {
      "name": "30 - 55 yr old",
      "value": 11
    },
    {
      "name": "55 -65 yr old",
      "value": 9
    },
    {
      "name": "> 65 yr old",
      "value": 4
    }
  ];

  singleMarker = [
    {
      "name": "Body Mass Index",
      "value": 7
    },
    {
      "name": "Blood Pressure",
      "value": 11
    },
    {
      "name": "Enzymatic",
      "value": 9
    },
    {
      "name": "Cholesterol",
      "value": 4
    },
    {
      "name": "LDL Cholesterol",
      "value": 7
    },
    {
      "name": "HDL Cholesterol",
      "value": 11
    },
    {
      "name": "Cholesterol: HDL Ratio",
      "value": 13
    },
    {
      "name": "Triglycerides",
      "value": 6
    },
    {
      "name": "ALB-CREA Ratio",
      "value": 3
    },
    {
      "name": "Glomenular",
      "value": 19
    },
  ];

  referenceLines = [
    {"name": "OVERWEIGHT", "value": 25},
    {"name": "HEALTHY", "value": 20},
    {"name": "UNDERWEIGHT", "value": 18}
  ]

  multi = [
    {
      "name": "No of Individual",
      "series": [
        {
          "name": "Individual 1",
          "value": 30
        },
        {
          "name": "Individual 2",
          "value": 10
        },
        {
          "name": "Individual 3",
          "value": 4
        },
        {
          "name": "Individual 4",
          "value": 7
        },
        {
          "name": "Individual 5",
          "value": 13
        },
        {
          "name": "Individual 6",
          "value": 23
        },
        {
          "name": "Individual 7",
          "value": 33
        },
      ]
    }
  ];

  // options
  view: any[] = [];
  viewbar: any[] = [ , 650];
  gradient: boolean = false;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'right';
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Country';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = 'Population';
  timeline: boolean = true;
  showRefLines: boolean = true;

  

  colorScheme = {
    domain: ['#029899', '#8ad056', '#006fc2', '#807d81']
  };

  account = {
    account_id: 1,
    account_no: null,
    individual_id: '741201-02-4567',
    package_id: 2,
    account_type: 2,
    expiry: '2022-07-05'
  }

  
 
  constructor(
    private router: Router,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private dataService: DataService,
    public translate: TranslateService
  ) { 
  }

  ngOnInit() {
  }

 
  mode:any;
  dataForm:FormGroup;
 
  dataMarker:any;
  tabClick(evt){
    this.dataMarker = this.dataMarker[evt.index];
    //this.getLatestResult();
  }


  //
  // NCD Prevalence Profile Expand Summary
  //
  NCDDetail: any;
  expandSummary(template: TemplateRef<any>, data) {
    //Get detail of NCD by ID (data)
    this.NCDDetail = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  markerName= 'Body Mass Index';
  NCDModalView:any[] = [];
  showMarker(marker: any) {
    this.markerName = marker;
  }

  //
  // NCD Prevalence Profile View Summary
  //
  summaryDetail: any;
  viewSummary(template: TemplateRef<any>, data) {
    this.summaryDetail = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

}
