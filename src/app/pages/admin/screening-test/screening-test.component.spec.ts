import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreeningTestComponent } from './screening-test.component';

describe('ScreeningTestComponent', () => {
  let component: ScreeningTestComponent;
  let fixture: ComponentFixture<ScreeningTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreeningTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreeningTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
