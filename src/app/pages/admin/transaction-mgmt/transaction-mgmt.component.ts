import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';
import {TranslateService} from '@ngx-translate/core';


declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-transaction-mgmt',
  templateUrl: './transaction-mgmt.component.html',
  styleUrls: [
    './transaction-mgmt.component.scss'
  ]
})
export class TransactionMgmtComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public translate: TranslateService
  ) {}


  // dataRegistration: any;
  modalRef: BsModalRef;
  loading = true;


  ngOnInit() {
    // this.fetchRegistration();
    this.fetchTransaction();
  }

  dataTransaction: any;
  fetchTransaction() {
    this.loading = true;
    this.dataTransaction = [];
    this.dataService.getAllTransactions().subscribe(res => {
      this.dataTransaction = res.data;
      this.loading = false;
      this.dataSearchTransaction = null;
      this.keyword = null;
    }, error => {
      this.loading = false;
    })
  }

  dataSearchTransaction: any;
  keyword: any;
  isSearching: any;
  searchTransaction() {
    this.isSearching = true;
    this.dataService.searchTransactions(this.keyword).subscribe(res => {
      this.dataSearchTransaction = res.data;
      this.isSearching = false;
    }, error => {
      this.isSearching = false;
    })
  }

}
