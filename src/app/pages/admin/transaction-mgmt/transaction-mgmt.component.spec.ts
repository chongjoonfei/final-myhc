import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionMgmtComponent } from './transaction-mgmt.component';

describe('TransactionMgmtComponent', () => {
  let component: TransactionMgmtComponent;
  let fixture: ComponentFixture<TransactionMgmtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionMgmtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionMgmtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
