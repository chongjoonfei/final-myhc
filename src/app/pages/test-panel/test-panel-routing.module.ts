import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestGroupComponent } from './group/test-group.component';
import { TestInputResultComponent } from './input-result/test-input-result.component';
import { TestManageComponent } from './manage/test-manage.component';
import { TestResultComponent } from './result/test-result.component';

const routes: Routes = [
  {
    path: 'result',
    component: TestResultComponent,
  },
  {
    path: 'manage',
    component: TestManageComponent,
  },
  {
    path: 'group',
    component: TestGroupComponent,
  },
  {
    path: 'input-result',
    component: TestInputResultComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestPanelRoutingModule { }
