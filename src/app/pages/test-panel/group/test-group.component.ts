import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from "lodash";
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-test-group',
  templateUrl: './test-group.component.html',
  styleUrls: ['./test-group.component.scss']
})
export class TestGroupComponent implements OnInit {

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) { }

  mode: any;
  modalRef: BsModalRef;
  isLoading = true;
  isSubmitted = false;

  ngOnInit() {

    window["sceop"]=this;
    this.fetchData();
    this.fetchTestPanel();
    this.fetchLocation();
    this.fetchPatientType();
    this.fetchPackageCategory();
    this.fetchRoleBased();

  }

  iTemsPermission:any=[];
  iMainId:any=[];
  RoleBase:any=[];
  fetchRoleBased()
  {

    let auth = JSON.parse(localStorage.getItem("auth"));
     // Get the Menu Owner
    this.dataService.getMainMenu(auth.account.menu_owner).subscribe(response=> {
      //console.log(response.data);
      for (var key in response.data) {
        for(var key2 in response.data[key]['items'])
        {
          var uqk = response.data[key]['items'][key2]['path'] +'/'+response.data[key]['items'][key2]['page'];
          this.iTemsPermission[uqk] = response.data[key]['items'][key2]['item_id'];
          var mainid = response.data[key]['items'][key2]['item_id'];
          this.iMainId[mainid] = response.data[key]['main_id'];
         }
      }
      var ItemId = this.iTemsPermission['/test-panel/group'];
      var MainId = this.iMainId[ItemId];
      this.dataService.getMenuItemById(ItemId, MainId).subscribe(res => {
      this.RoleBase = res;

      });

    });

  }

  fetchData() {
    this.dataService.getAllAddOnPackage().subscribe(result => {
      if (result.data) this.dataAddOnP = result.data;
      this.isLoading = false;
    });

  }

  selectedPackage: any;
  dataForm: FormGroup;
  dataFormTestGroup: FormGroup;
  dataFormAddOnP: FormGroup;
  dataTestGroup: any;
  dataAddOnP: any;

  newAddOnPackage(template: TemplateRef<any>) {
    this.mode = "NEW";
    this.dataFormAddOnP = this.formBuilder.group({
      "test_group_code": ['AOT'],
      "package_category": null,
      "group_name": [null, Validators.required],
      "patient_type": [null, Validators.required],
      "price": [null, Validators.required],
      "enabled": ["YES", Validators.required]

    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveAddOnPackage() {
    this.isSubmitted = true;
    if (this.dataFormAddOnP.invalid) {
      this.translate.get('test-panel').subscribe(lang => {
        this.toastrService.warning(lang.group.notification.title_incompleteDetails, lang.group.notification.title_warning);
      });
      return;
    }
    this.translate.get('test-panel').subscribe(lang => {
    this.coolDialogs.confirm(lang.group.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }

            this.dataService.createAddOnPackage(this.dataFormAddOnP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.group.notification.title_warning); else {
                this.toastrService.success(data.message, lang.group.notification.title_success);
                this.isSubmitted = false;

                this.mode = "EDIT";
                this.dataFormAddOnP = this.formBuilder.group({
                  "test_group_code": [data.content.test_group_code, Validators.required],
                  "package_category": data.content.package_category,
                  "group_name": [data.content.group_name, Validators.required],
                  "patient_type": [data.content.patient_type, Validators.required],
                  "price": [data.content.price, Validators.required],
                  "enabled": [data.content.enabled, Validators.required]
                });
                this.fetchData();
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error(lang.group.notification.title_dataExistPackage, lang.group.notification.title_warning);
              else
                this.toastrService.error(lang.group.notification.title_serviceUnavailablePackage, lang.group.notification.title_warning);
            });
          } else {
            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }

            this.dataService.updateAddOnPackage(this.dataFormAddOnP.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.group.notification.title_warning); else {
                this.toastrService.success(data.message, lang.group.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error(lang.group.notification.title_failedUpdatePlanPackage, lang.group.notification.title_warning);
            });
          }
        }
      });
    });
  }

  editAddOnPackage(template: TemplateRef<any>, data) {
    this.selectedPackage = data;
    this.mode = "EDIT";
    this.dataFormAddOnP = this.formBuilder.group({
      "test_group_code": [data.test_group_code, Validators.required],
      "package_category": data.package_category,
      "group_name": [data.group_name, Validators.required],
      "patient_type": [data.patient_type, Validators.required],
      "price": [data.price, Validators.required],
      "enabled": [data.enabled, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  deleteAddOnPackage(code) {
    this.translate.get('test-panel').subscribe(lang => {
    this.coolDialogs.confirm(lang.group.notification.title_deleteRecord + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          this.dataService.deleteAddOnPackage(code).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.group.notification.title_warning); else {
              this.toastrService.success(data.message, lang.group.notification.title_success);
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.group.notification.title_failedDeleteScreeningRecord, lang.group.notification.title_warning);
      });
    });
  }

  dataFormTestPanel: FormGroup;
  dataTestPanel: any;
  modeTPanel: any;

  editTestPanel(test_group_code) {
    this.modeTPanel = "EDIT";
    this.dataFormTestPanel = this.formBuilder.group(test_group_code);
  }

  newTestPanel(test_group_code) {
    this.modeTPanel = "NEW";
    this.dataFormTestPanel = this.formBuilder.group({
      "test_group_code": [test_group_code, Validators.required],
      "test_panel_code": [null, Validators.required]
    });
  }

  deleteTestPanel(tp) {
    this.translate.get('test-panel').subscribe(lang => {
      this.coolDialogs.confirm(lang.group.notification.title_deleteRecord + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          let deleteTestPanel = {
            "test_group_code": this.selectedPackage.test_group_code,
            "test_panel_code": tp.test_panel_code
          };
          this.dataService.deleteTestPanelInAddOnPackage(deleteTestPanel).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.group.notification.title_warning); else {
              this.toastrService.success(data.message, lang.group.notification.title_success);
              this.dataService.getAddOnPackageOne(this.selectedPackage.test_group_code).subscribe(data => {
                this.selectedPackage = data;
              });
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.group.notification.title_failedDeleteTestPanel, lang.group.notification.title_warning);
      });
    });
  }

  addTestPanel() {
    this.isSubmitted = true;
    if (this.dataFormTestPanel.invalid || this.dataFormTestPanel.value.test_panel_code=='null') {
      this.translate.get('test-panel').subscribe(lang => {
        this.toastrService.warning(lang.group.notification.title_incompleteDetails, lang.group.notification.title_warning);
      });
      return;
    }

    this.translate.get('test-panel').subscribe(lang => {
    this.coolDialogs.confirm(lang.group.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if (this.modeTPanel == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.createTestPanelInAddOnPackage(this.dataFormTestPanel.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.group.notification.title_warning); else {
                this.toastrService.success(data.message, lang.group.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.dataService.getAddOnPackageOne(this.selectedPackage.test_group_code).subscribe(data => {
                  this.selectedPackage = data;
                });
                this.modeTPanel = null;

              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error(lang.group.notification.title_serviceUnavailableTestPanel, lang.group.notification.title_warning);
              else
                this.toastrService.error(lang.group.notification.title_failedDeleteTestPanel, lang.group.notification.title_warning);
            });
          } else {
            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.updateAddOnPackage(this.dataFormTestPanel.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.group.notification.title_warning); else {
                this.toastrService.success(data.message, lang.group.notification.title_success);
                this.isSubmitted = false;
                this.dataService.getAddOnPackageOne(this.selectedPackage.test_group_code).subscribe(data => {
                  this.selectedPackage = data;
                });
                // this.fetchData();
                this.modalRef.hide();
                this.modeTPanel = null;
              }
            }, error => {
              this.toastrService.error(lang.group.notification.title_failedAddTestPanel, lang.group.notification.title_warning);
            });
          }
        }
      });
    });
  }


  // Lookups

  locationTypes: any = [];
  fetchLocation() {
    this.dataService.getLLocationType().subscribe(result => {
      if (result.data) this.locationTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  patientTypes: any = [];
  fetchPatientType() {
    this.dataService.getLPatientType().subscribe(result => {
      if (result.data) this.patientTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  testPanelTypes: any = [];
  fetchTestPanel() {
    this.dataService.getAllTestPanel().subscribe(result => {
      if (result.data) this.testPanelTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  packageCategoryTypes: any[];
  fetchPackageCategory() {
    this.dataService.getAllPackageCategory().subscribe(result => {
      if (result.data) this.packageCategoryTypes = result.data;
    }, error => {
      this.isLoading = false;
    })
  }

  cancel() {
    this.modeTPanel = null;
    this.modalRef.hide();
    this.fetchData();
  }



}
