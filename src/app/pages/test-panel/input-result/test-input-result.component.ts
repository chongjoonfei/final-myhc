import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray, ValidationErrors, AbstractControl, ValidatorFn } from '@angular/forms';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from "lodash";
import { DatePipe } from '@angular/common';

export function customValidateGroup(): ValidatorFn {
  return (formArray: FormArray): { [key: string]: any } | null => {
    let valid: boolean = true;
    formArray.controls.forEach((x: FormGroup,i) => {
      var sep = String(23.32).match(/\D/)[0];
      var b = String(x.value.test_value).split(sep);
      let countDec = b[1] ? b[1].length : 0;
      valid = valid && ((x.value.test_marker_format!=0 && x.value.test_marker_format == countDec) || x.value.test_value==0 || x.value.test_value==null || x.value.test_value=='' || x.value.test_marker_format==0);
      console.log(i+": " + valid);
    })
    return valid ? null : { error: 'Invalid format' }
  }
};

@Component({
  selector: 'app-test-input-result',
  templateUrl: './test-input-result.component.html',
  styleUrls: ['./test-input-result.component.scss']
})
export class TestInputResultComponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private datePipe: DatePipe
  ) {

  }

  isLoading = true;
  modalRef: BsModalRef;

  dataAllPatients: any;
  dataAllTestPanel: any;
  dataAllResult: any;
  ngOnInit() {
    this.fetchRoleBased();
    window["inutScope"]=this;
    this.dataService.getAllPatients().subscribe(res => {
      this.dataAllPatients = res.data;
    });
    this.dataService.getAllTestPanel().subscribe(res => {
      this.dataAllTestPanel = res.data;
    });

  }
  iTemsPermission:any=[];
  iMainId:any=[];
  RoleBase:any=[];
  fetchRoleBased()
  {
   
    let auth = JSON.parse(localStorage.getItem("auth"));
     // Get the Menu Owner
    this.dataService.getMainMenu(auth.account.menu_owner).subscribe(response=> {
      //console.log(response.data);
      for (var key in response.data) {
        for(var key2 in response.data[key]['items'])
        {
          var uqk = response.data[key]['items'][key2]['path'] +'/'+response.data[key]['items'][key2]['page'];
          this.iTemsPermission[uqk] = response.data[key]['items'][key2]['item_id'];
          var mainid = response.data[key]['items'][key2]['item_id'];
          this.iMainId[mainid] = response.data[key]['main_id'];
         }
      }
      var ItemId = this.iTemsPermission['/test-panel/group'];
      var MainId = this.iMainId[ItemId];
      this.dataService.getMenuItemById(ItemId, MainId).subscribe(res => { 
      this.RoleBase = res;
      console.log(res);
      });
      
    });
    
  }





  getCurrentDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    return yyyy + '-' + mm + '-' + dd;
  }


  optFilter: any = "test date";
  fetchResult() {
    this.dataService.getAllTestResult().subscribe(res => {
      this.dataAllResult = res;
    });
  }

  filter_test_date: any;
  filter_patient: any;
  filter_ic: any;
  noteFilter: any;
  loading = false;
  filter() {
    this.dataAllResult = null;
    this.noteFilter = null;
    let key = null;
    if (this.optFilter == 'test date') {

      if (!this.filter_test_date) {
        this.toastrService.warning("Please enter a valid value for test date");
        return;
      }
      key = this.datePipe.transform(this.filter_test_date, 'yyyy-MM-dd');
      this.noteFilter = "Result by test date " + key;
    } else {
      if (!this.filter_patient) {
        this.toastrService.warning("Please enter a valid value for IC No");
        return;
      }
      key = this.filter_patient.ic_no;
      this.ic_no = this.filter_patient.ic_no;
      this.noteFilter = "Result for " + this.filter_patient.name + " (" + this.filter_patient.ic_no + ")";
    }
    this.loading = true;
    this.dataService.searchTestResult(key).subscribe(res => {
      this.dataAllResult = res;
      this.loading = false;
    }, error => {
      this.loading = false;
    });
  }

  dataTestResult: FormGroup;
  ic_no: any;
  mode: any;
  newTestResult(template: TemplateRef<any>) {
    this.isSubmitted=false;
    this.mode = "NEW";
    this.selectedTestMarker = null;
    this.selectedPatient = null;
    this.ic_no = null;
    this.patient = null;
    this.dataFormMarker = null;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }

  updateTestResult(template: TemplateRef<any>, data) {
    this.isSubmitted=false;
    this.mode = "EDIT";
    this.ic_no = data.test_markers[0].patient_ic_no;
    this.selectedPatient = this.getPatient(this.ic_no);
    this.selectedTestMarker = this.getTestPanel(data.test_panel_code);

    // console.log("this.selectedTestMarker",this.selectedTestMarker);

    this.dataFormMarker = this.formBuilder.group({
      ic_no: this.ic_no,
      test_date: data.test_markers[0].test_date,
      test_panel_code: this.selectedTestMarker.test_panel_code,
      results: this.formBuilder.array([], 
        {
        validators: [customValidateGroup()]
        })
    })

    this.selectedTestMarker.test_markers.forEach(marker => {
      this.dataFormMarker.get('results').push(this.editMarkerList(marker, data.test_markers));
    })

    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-xl' });
  }


  editMarkerList(marker, result_markers) {
 
    let result = _.filter(result_markers, { test_marker_code: marker.code });

    let fg = this.formBuilder.group(
      {
        patient_ic_no: this.ic_no,
        booking_no: null,
        test_marker_code: marker.code,
        test_marker_name: marker.name,
        test_marker_unit: marker.unit,
        test_marker_format: marker.data_format,
        test_panel_code: marker.test_panel_code,
        reg_no: null,
        test_date: result.length > 0 ? result[0].test_date : null,
        test_value: result.length > 0 ? result[0].test_value : null,
        source: 'MANUAL'
      }
    );

    return fg;
  }

  getPatient(ic_no) {
    let patient = _.filter(this.dataAllPatients, { ic_no: ic_no });
    if (patient.length > 0) {
      return (patient[0].person);
    } else {
      return null;
    }
  }

  getTestPanel(test_panel_code) {
    let test_panel = _.filter(this.dataAllTestPanel, { test_panel_code: test_panel_code });
    if (test_panel.length > 0) {
      return (test_panel[0]);
    } else {
      return null;
    }
  }

  selectedTestMarker: any;
  dataFormMarker: any;
  selectTestMarker(panel) {
    this.selectedTestMarker = panel;
    this.dataFormMarker = this.formBuilder.group({
      ic_no: this.selectedPatient.ic_no,
      test_date: null,
      test_panel_code: this.selectedTestMarker.test_panel_code,
      results: this.formBuilder.array([panel.test_markers], {
        validators: [customValidateGroup()]
        })
    })

    let items = this.dataFormMarker.get('results') as FormArray;
    while (items.length) {
      items.removeAt(0);
    }


    this.selectedTestMarker.test_markers.forEach(marker => {
      this.dataFormMarker.get('results').push(this.createMarkerList(marker));
    })
  }

  createMarkerList(marker) {
    return this.formBuilder.group(
      {
        patient_ic_no: this.selectedPatient.ic_no,
        booking_no: null,
        test_marker_code: marker.code,
        test_marker_name: marker.name,
        test_marker_unit: marker.unit,
        test_marker_format: marker.data_format,
        test_panel_code: this.selectedTestMarker.test_panel_code,
        reg_no: null,
        test_date: null,
        test_value: null,
        source: 'MANUAL'
      } );
  }

  selectedPatient: any;
  setPatient(ic_no) {
    let patient = _.filter(this.dataAllPatients, { ic_no: ic_no });
    this.selectedPatient = patient[0].person;
    // console.log(this.selectedPatient);
  }

  selectPatient(event) {
    // console.log(this.filter_patient);
    this.selectedPatient = event;
  }

  onSearchPatient(filterType) {
    this.optFilter = filterType;
  }

  filteredSingle: any[];
  patient: any;
  filterSingle(event) {
    let query = event.query;
    // console.log(query,this.dataAllPatients);
    this.filteredSingle = this.filterPatient(query, this.dataAllPatients);
  }

  filterPatient(query, patients: any[]): any[] {
    let filtered: any[] = [];
    for (let i = 0; i < patients.length; i++) {
      let patient = patients[i];
      if ((patient.person.name && patient.person.name.toLowerCase().indexOf(query.toLowerCase()) == 0) || (patient.person.ic_no && patient.person.ic_no.toLowerCase().indexOf(query.toLowerCase()) == 0)) {
        filtered.push(patient.person);
      }
    }
    return filtered;
  }

  isSubmitted=false;
  saveInput() {
    this.isSubmitted = true;
    if (!this.dataFormMarker.valid){
      this.toastrService.warning("Please make sure all input values are valid", "Warning");
      return;
    }
    
    this.coolDialogs.confirm("Are you sure to save this input?")
      .subscribe(confirm => {
        if (confirm) {
          

          let dataFormValue = this.dataFormMarker.value;

          dataFormValue.results.forEach(element => {
            element.test_date = this.datePipe.transform(dataFormValue.test_date, 'yyyy-MM-dd');
          });

          this.dataService.pushTestResult(dataFormValue.results).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error(res.message, "Failed");
            } else {
              this.filter_patient = this.selectedPatient;
              this.optFilter = "IC No";
              this.filter();
              this.modalRef.hide();
              this.toastrService.success("Test result has been successfully saved", "Success");
            }
          }, error => {
            this.toastrService.error("Unable to save test result", "Failed");
          });

        }
      });
  }

  deleteResult(data) {
    this.coolDialogs.confirm("Are you sure to delete this input?")
      .subscribe(confirm => {
        if (confirm) {
          let deleteData = {
            patient_ic_no: data.value.test_markers[0].patient_ic_no,
            test_panel_code: data.value.test_markers[0].test_panel_code,
            test_date: data.value.test_markers[0].test_date
          };
          // console.log(deleteData);

          this.dataService.deleteTestResult(deleteData).subscribe(res => {
            if (res.errorFound) {
              this.toastrService.error("Could not delete the test result", "Process failed");
            } else {
              this.filter_patient = data.value.test_markers[0].patient_ic_no;
              this.optFilter = "IC No";
              this.filter();
              this.toastrService.success("The test result has successfully deleted", "Success");
            }
          }, error => {
            this.toastrService.error("Could not delete the test result", "Process failed");
          });

        }
      });
  }

  isValidDecimalPlaces(num, noOfDecimalPoint) {
    
    if(num=='' || num==null)return true;
    var sep = String(23.32).match(/\D/)[0];
    var b = String(num).split(sep);
    let countDec = b[1] ? b[1].length : 0;
    return (noOfDecimalPoint == countDec);
  }

  


}
