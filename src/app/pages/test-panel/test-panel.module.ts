import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import {SharedModule} from '../../shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ToastrModule } from 'ngx-toastr';
import { TestPanelRoutingModule } from './test-panel-routing.module';
import { MaterialModule } from '../../shared/material-module';
import { TestResultComponent } from './result/test-result.component';
import { TestManageComponent } from './manage/test-manage.component';
import { FindPipe } from '../../shared/pipe/find.pipe';
// import { TooltipModule } from 'ngx-bootstrap';
import { TestInputResultComponent } from './input-result/test-input-result.component';
import { TestGroupComponent } from './group/test-group.component';


@NgModule({
  imports: [
    CommonModule,
    TestPanelRoutingModule,
    SharedModule,
    NgxChartsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    MaterialModule,
    TranslateModule
  ],
  exports: [
    TranslateModule
  ],
  declarations: [TestResultComponent,TestManageComponent, TestGroupComponent ,TestInputResultComponent, FindPipe]
})
export class TestPanelModule { }
