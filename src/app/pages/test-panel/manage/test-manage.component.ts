import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from "lodash";


@Component({
  selector: 'app-test-manage',
  templateUrl: './test-manage.component.html',
  styleUrls: ['./test-manage.component.scss']
})
export class TestManageComponent implements OnInit {

  constructor(private dataService: DataService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    public translate: TranslateService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService
  ) {

  }

  isLoading = true;
  modalRef: BsModalRef;
  dataForm: FormGroup;
  dataFormPanel: FormGroup;
  dataFormMarker: FormGroup;
  dataFormRefRange: FormGroup;

  testMarkerRef = [
    { code: 'Low' },
    { code: 'Normal' },
    { code: 'Borderline' },
    { code: 'High' },
    { code: 'Very High' }
  ]

  dataTestPanel: any;
  dataTestMarker: any;
  dataRefRange: any;

  ngOnInit() {
    this.fetchRoleBased();
    this.fetchData();
  }

  iTemsPermission:any=[];
  iMainId:any=[];
  RoleBase:any=[];
  fetchRoleBased()
  {

    let auth = JSON.parse(localStorage.getItem("auth"));
     // Get the Menu Owner
    this.dataService.getMainMenu(auth.account.menu_owner).subscribe(response=> {
      //console.log(response.data);
      for (var key in response.data) {
        for(var key2 in response.data[key]['items'])
        {
          var uqk = response.data[key]['items'][key2]['path'] +'/'+response.data[key]['items'][key2]['page'];
          this.iTemsPermission[uqk] = response.data[key]['items'][key2]['item_id'];
          var mainid = response.data[key]['items'][key2]['item_id'];
          this.iMainId[mainid] = response.data[key]['main_id'];
         }
      }
      var ItemId = this.iTemsPermission['/test-panel/manage'];
      var MainId = this.iMainId[ItemId];
      this.dataService.getMenuItemById(ItemId, MainId).subscribe(res => {
      this.RoleBase = res;

      });

    });

  }

  fetchData() {
    this.dataService.getAllTestPanel().subscribe(result => {
      if (result.data) this.dataTestPanel = result.data;
      // this.getSingleMarkerRecord();
      if (this.selectedMarkerRefRange) {
        let panel = _.filter(this.dataTestPanel, { test_panel_code: this.selectedMarkerRefRange.test_panel_code });
        // console.log("panel", panel);
        let markerRefRange = _.find(panel[0].test_markers, { code: this.selectedMarkerRefRange.code });
        // console.log("markerRefRange", markerRefRange);
        this.selectedMarkerRefRange.test_reference_range = [...markerRefRange.test_reference_range];
        this.modeRange = null;
      }
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });

  }

  isSubmitted = false;
  mode = "NEW";
  editTestPanel(template: TemplateRef<any>, data) {
    this.mode = "EDIT";
    this.dataFormPanel = this.formBuilder.group({
      "panel_id": [data.panel_id, Validators.required],
      "name": [data.name, Validators.required],
      "test_panel_code": [data.test_panel_code, Validators.required],
      "description": [data.description],
      "input_type": [data.input_type, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  savePanel() {
    this.isSubmitted = true;
    let formValue = this.dataFormPanel.value;
    if (this.dataFormPanel.invalid) {
      this.translate.get('test-panel').subscribe(lang => {
        this.toastrService.warning(lang.manage.notification.title_incompleteDetails, lang.manage.notification.title_warning);
      });
      return;
    }

    this.translate.get('test-panel').subscribe(lang => {
    this.coolDialogs.confirm(lang.manage.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.update==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          this.dataService.updateTestPanel(this.dataFormPanel.value).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.manage.notification.title_warning); else {
              this.toastrService.success(data.message, lang.manage.notification.title_success);
              this.isSubmitted = false;
              this.fetchData();
              this.modalRef.hide();
            }
          }, error => {
            this.toastrService.error(lang.manage.notification.title_failedUpdateTestPanel, lang.manage.notification.title_warning);
          });
        }
      });
    });
  }

  selectedTestPanel:any;
  newTestMarker(template: TemplateRef<any>,testPanel) {
    this.selectedTestPanel = testPanel;
    this.mode = "NEW";
    this.dataFormMarker = this.formBuilder.group({
      "test_panel_code": [testPanel.test_panel_code, Validators.required],
      "name": [null, Validators.required],
      "code": [null, Validators.required],
      "description": [null, Validators.required],
      "unit": [null, Validators.required],
      "data_format": [0, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  // editTestMarker(template: TemplateRef<any>, data) {
  //   this.mode = "EDIT";
  //   this.dataFormMarker = this.formBuilder.group(data);
  // }

  editTestMarker(template: TemplateRef<any>, data,testPanel) {
    this.selectedTestPanel = testPanel;
    this.mode = "EDIT";
    this.dataFormMarker = this.formBuilder.group({
      "test_panel_code": [data.test_panel_code, Validators.required],
      "name": [data.name, Validators.required],
      "code": [data.code, Validators.required],
      "description": [data.description],
      "unit": [data.unit],
      "data_format": [data.data_format, Validators.required]
    });
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }

  saveMarker() {
    this.isSubmitted = true;
    let formValue = this.dataFormMarker.value;
    if (this.dataFormMarker.invalid) {
      this.translate.get('test-panel').subscribe(lang => {
        this.toastrService.warning(lang.manage.notification.title_incompleteDetails, lang.manage.notification.title_warning);
      });
      return;
    }

    this.translate.get('test-panel').subscribe(lang => {
    this.coolDialogs.confirm(lang.manage.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if (this.mode == "NEW") {
            if(this.RoleBase.create==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.createTestMarker(this.dataFormMarker.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.manage.notification.title_warning); else {
                this.toastrService.success(data.message, lang.manage.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
                // this.mode = "EDIT";
              }
            }, error => {
              if (error.status == 409)
                this.toastrService.error(lang.manage.notification.title_dataExistTestMarker, lang.manage.notification.title_warning);
              else
                this.toastrService.error(lang.manage.notification.title_serviceUnavailableTestMarker, lang.manage.notification.title_warning);
            });
          } else {
            if(this.RoleBase.update==0)
            {
              this.translate.get('app').subscribe(data => {
                this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
              });
              return false;
            }
            this.dataService.updateTestMarker(this.dataFormMarker.value).subscribe(data => {
              if (data.errorFound) this.toastrService.error(data.message, lang.manage.notification.title_warning); else {
                this.toastrService.success(data.message, lang.manage.notification.title_success);
                this.isSubmitted = false;
                this.fetchData();
                this.modalRef.hide();
              }
            }, error => {
              this.toastrService.error(lang.manage.notification.title_failedAddTestMarker, lang.manage.notification.title_warning);
            });
          }
        }
      });
    });
  }


  selectedMarkerRefRange: any;
  formEditRefRange: FormGroup;
  editRefRange(template: TemplateRef<any>, data) {
    // console.log("selectedMarkerRefRange",data);
    this.selectedMarkerRefRange = data;
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  saveRefRange() {
    this.isSubmitted = true;
    let formValue = this.formEditRefRange.value;
    if (this.formEditRefRange.invalid) {
      this.translate.get('test-panel').subscribe(lang => {
        this.toastrService.warning(lang.manage.notification.title_incompleteDetails, lang.manage.notification.title_warning);
      });
      return;
    }

    if (!this.isValidDecimalPlaces(this.formEditRefRange.get('min').value)){
      this.translate.get('test-panel').subscribe(lang => {
        this.toastrService.warning(lang.manage.notification.title_invalidMinValue, lang.manage.notification.title_warning);
      });
      return;
    }

    if (!this.isValidDecimalPlaces(this.formEditRefRange.get('max').value)){
      this.translate.get('test-panel').subscribe(lang => {
        this.toastrService.warning(lang.manage.notification.title_invalidMaxValue, lang.manage.notification.title_warning);
      });
      return;
    }

    this.translate.get('test-panel').subscribe(lang => {
    this.coolDialogs.confirm(lang.manage.notification.title_saveInfo + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.update==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          this.dataService.updateRefRange(this.formEditRefRange.value).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.manage.notification.title_warning); else {
              this.toastrService.success(data.message, lang.manage.notification.title_success);
              this.isSubmitted = false;
              this.fetchData();
            }
          }, error => {
            this.toastrService.error(lang.manage.notification.title_faildUpdateRefRange, lang.manage.notification.title_warning);
          });
        }
      });
    });
  }

  modeRange: any;
  editRange(range) {
    this.modeRange = "Edit";
    // console.log(range)
    this.formEditRefRange = this.formBuilder.group(range);
    // this.fetchData();
  }

  newRange() {
    // console.log("selectedMarkerRefRange",this.selectedMarkerRefRange);
    this.modeRange = "New";
    this.formEditRefRange = this.formBuilder.group(
      {
        "test_marker_code": this.selectedMarkerRefRange.code,
        "code": null,
        "min": null,
        "max": null,
        "summary": null
      }
    );

  }

  deleteRange(data) {
    this.translate.get('test-panel').subscribe(lang => {
    this.coolDialogs.confirm(lang.manage.notification.title_deleteRefRange + "?")
      .subscribe(res => {
        if (res) {
          if(this.RoleBase.delete==0)
          {
            this.translate.get('app').subscribe(data => {
              this.toastrService.error(data.rbac.title_permissionDenied_desc, data.rbac.title_permissionDenied);
            });
            return false;
          }
          this.dataService.deleteRefRange(data).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, lang.manage.notification.title_warning); else {
              this.toastrService.success(data.message, lang.manage.notification.title_success);
              this.fetchData();
            }
          });
        }
      }, error => {
        this.toastrService.error(lang.manage.notification.title_faildDeleteRefRange, lang.manage.notification.title_warning);
      });
    });
  }

isValidDecimalPlaces(num) {
    var sep = String(23.32).match(/\D/)[0];
    var b = String(num).split(sep);
    let countDec = b[1]? b[1].length : 0;
    return (this.selectedMarkerRefRange.data_format==countDec);
}

}
