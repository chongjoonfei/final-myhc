import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../service/data.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { environment } from '../../../environments/environment';


@Component({
    selector: 'app-my-biodata',
    templateUrl: './my-biodata.component.html',
    styleUrls: ['./my-biodata.component.scss']
})

export class MyBiodataComponent {


    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private coolDialogs: NgxCoolDialogsService,
        private dataService: DataService,
        private toastrService: ToastrService,
    ) { }

    slideConfig = {
      "slidesToShow": 4,
      "slidesToScroll": 1,
      "nextArrow": "<div class='nav-btn next-slide'></div>",
      "prevArrow": "<div class='nav-btn prev-slide'></div>",
      "dots": true,
      "infinite": false
    };

    dataRegistration:any;
    qrActive: any;
    dataPatients:any=[];
    selectedPatientData: any = [];
    dataForm: any;
    user: any;
    auth: any;
    loading:any=true;
    ngOnInit() {
        this.fetchStateInfo();
        this.fetchBiodata();

        if (localStorage.getItem("plan-registration")){
          this.qrActive = 0;
          this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));
          this.dataPatients = this.dataRegistration.registration_persons;
          this.selectedPatientData = this.dataPatients[this.qrActive];
          console.log(this.selectedPatientData);
        }else{
          this.router.navigate(['/mau']);
        }

    }

    photoUrl:any;
    fetchBiodata(){
        this.loading = true;
        this.auth = JSON.parse(localStorage.getItem("auth"));
        this.dataService.getUserDetails(this.auth.username).subscribe(user => {
            this.user = user;
            this.photoUrl = environment.uploadPath + this.user.person.photo_path;
            // this.dataForm = this.formBuilder.group({
            //     name: [user.person.name, Validators.required],
            //     age: [user.person.age, Validators.required],
            //     ic_no: [user.person.ic_no, Validators.required],
            //     mobile_no: [user.person.mobile_no, Validators.required],
            //     email: [user.person.email, Validators.required],
            //     gender: [user.person.gender, Validators.required],
            //     patient_type_code: [user.person.patient_type_code],
            //     address: [user.person.address],
            //     town: [user.person.town],
            //     district: [user.person.district],
            //     postcode: [user.person.postcode],
            //     state: [user.person.state],
            //     status: [user.person.status],
            //     reg_no: user.reg_no
            // });
            // console.log("user", user,this.dataForm.value);

            this.loading = false;
        }, error => {
            this.loading = false;
        });
    }

    showProfile(index) {
      this.qrActive = index;
      this.selectedPatientData = this.dataPatients[index];
    }
    
    states: any;
    fetchStateInfo() {
      this.dataService.getStates().subscribe(res => {
        this.states = res.data;
      });
    }

    // isSubmitted:any;
    // saveBiodata(){
    //     this.isSubmitted = true;
    //     if (this.dataForm.invalid){
    //         this.toastrService.warning("Please complete the form details before submitting", "Incomplete information");
    //         return;
    //     }
    //     this.coolDialogs.confirm("Are you sure to update this information?")
    //     .subscribe(res => {
    //       if (res) {
    //         let person = this.dataForm.value;
    //         this.dataService.saveUpdatePerson(person).subscribe(reg => {
    //           this.toastrService.success("Your biodata has been updated", "Success");
    //           this.fetchBiodata();
    //           this.isSubmitted=false;
    //         }, msg1 => {
    //           this.toastrService.error(msg1.error.message, "Failed");
    //           this.isSubmitted=false;
    //         });
    //       }
    //     });
    // }

    cpass:any;
    npass:any;
    rpass:any;
    changePassword(){
        if (this.npass!=this.rpass){
            this.toastrService.warning("Sorry, your new password and re-type password does not matched", "Password does not matched");
        }else{
            let data = { "username": this.user.username, "cpass": this.cpass, "npass": this.npass };
            this.dataService.changePassword(data).subscribe(res => {
                  this.toastrService.success("Your password has been updated", "Success");
                  this.cpass=null;
                  this.npass=null;
                  this.rpass=null;
                this.fetchBiodata();
              }, msg1 => {
                this.toastrService.error(msg1.error.message, "Failed");
              });
        }
    }

    setDefaultPic(event) {
        event.target.src = "assets/images/avatar.jpg";
    }

}


