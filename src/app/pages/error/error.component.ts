import { AfterViewInit, ChangeDetectorRef, ElementRef, Input, TemplateRef, ViewChild } from '@angular/core';
import { Component, OnInit  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { ActivatedRoute, Router } from '@angular/router';
 

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html'
 
})
export class ErrorComponent implements OnInit {

  constructor( private route: ActivatedRoute,) { }
  
  title:any;
  message:any;
  ngOnInit() {
    let errorCode = this.route.snapshot.paramMap.get('code');

    if (errorCode=='payment-failed'){
      this.title = "Online Payment Failure";
      this.message = "Your transaction cannot be completed.";
    }
  }
 

}
