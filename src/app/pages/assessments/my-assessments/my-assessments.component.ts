import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../service/data.service';
import * as _ from "lodash";
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-my-assessments',
  templateUrl: './my-assessments.component.html',
  styleUrls: [
    './my-assessments.component.scss' 
  ]
})
export class MyAssessmentsComponent implements OnInit {
  
  user:any;
  auth:any;
  
  constructor(private dataService: DataService,public translate: TranslateService) {}

  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));

    this.dataService.getUserDetails(this.auth.username).subscribe(user=> {
      this.user = user;
    });
  }

  
}

 