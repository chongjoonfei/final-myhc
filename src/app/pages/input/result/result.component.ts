import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { LineChartComponent } from '@swimlane/ngx-charts';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../service/data.service';
import { CustomLinerChartService } from '../../../service/CustomLinerChartService';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  /** test graf */

  graphData: any = [];
  graphLegend: any = [];

  ldlMonths = [{ no: 1 }, { no: 2 }, { no: 3 }, { no: 4 }, { no: 5 }, { no: 6 }, { no: 7 }, { no: 8 }, { no: 9 }, { no: 10 }, { no: 11 }, { no: 12 }];
  legendLDL = [
    { name: "Optimal", value: "2.58 - 3.34", color: "#ABEBC6", min: 2.58, max: 3.34 },
    { name: "Borderline", value: "3.34 - 4.11", color: "#6495ED", min: 3.34, max: 4.11 },
    { name: "High", value: "4.11 - 4.89", color: "#FAD7A0", min: 4.11, max: 4.89 },
    { name: "Very High", value: "> 4.89", color: "#F5B7B1", min: 4.90, max: 5.5 }
  ];
  graphDataLDL: any[] = [
    {
      "name": "LDL Reading",
      "series": []
    }
  ];

  hbA1cMonths = [{ no: 3 }, { no: 6 }, { no: 9 }, { no: 12 }, { no: 15 }, { no: 18 }, { no: 21 }, { no: 24 }, { no: 27 }, { no: 30 }, { no: 33 }, { no: 36 }];
  legendHbA1c = [
    { name: "Normal", value: "< 5.7%", color: "#ABEBC6", min: 4.50, max: 5.69 },
    { name: "Pre-Diabetes", value: "5.7 - 6.2%", color: "#6495ED", min: 5.70, max: 6.19 },
    { name: "Diabetes", value: "> 6.3%", color: "#F5B7B1", min: 6.20, max: 7.50 }
  ];
  graphDataHbA1c: any[] = [
    {
      "name": "HbA1c Reading",
      "series": []
    }
  ];
  view: any[] = [600, 250];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Month';
  yAxisLabel: string = 'mmol/L';
  timeline: boolean = false;

  colorScheme: any;
  //black, red, orange, blue,orange,green
  colorSchemeLDL = {
    domain: ['#000000']
  };

  //black, green, orange, red
  colorSchemeHbA1c = {
    domain: ['#000000']
  };
  /**end test graf */


  closeResult: string;

  constructor(private modalService: BsModalService,
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,
    private customLinerChartService: CustomLinerChartService,
    private coolDialogs: NgxCoolDialogsService) { }

  modalRef: BsModalRef;
  isLoading = true;

  // @ViewChild('chart',{static:false}) chart: LineChartComponent;



  ngOnInit() {
    this.fetchPatientsData();

    //generate graphDataHbA1c
    this.legendHbA1c.forEach(legend => {
      this.colorSchemeHbA1c.domain.push(legend.color);
      let borderline = {
        "name": legend.name,
        "series": []
      };
      this.hbA1cMonths.forEach(month => {
        borderline.series.push({ name: month.no, value: legend.min, min: legend.min, max: legend.max })
      })
      this.graphDataHbA1c.push(borderline);
    });


    //generate graphDataLDL
    this.legendLDL.forEach(legend => {
      this.colorSchemeLDL.domain.push(legend.color);
      let borderline = {
        "name": legend.name,
        "series": []
      };
      this.ldlMonths.forEach(month => {
        borderline.series.push({ name: month.no, value: legend.min, min: legend.min, max: legend.max })
      })
      this.graphDataLDL.push(borderline);
    });


  }

  dataPatients: any = [];
  fetchPatientsData() {
    this.dataService.getAllPatients().subscribe(data => {
      if (data.records) this.dataPatients = data.records;
      this.isLoading = false;
    }, error => {
      this.isLoading = false;
    });
  }

  searchKey: any = "";
  searchPatientsData() {
    this.isLoading = true;
    this.dataService.searchPatientInfo(this.searchKey).subscribe(data => {
      if (data.records) this.dataPatients = data.records;
      this.isLoading = false;
    }, error => {
      this.dataPatients = [];
      this.isLoading = false;
    });
  }

  dataLDLHealthResult: any = [];
  ldlResult: any = [];
  fetchLDLHealthData(refno) {
    this.dataLDLHealthResult = [];
    this.ldlResult = [];
    this.graphData = [];
    this.dataService.getHealthResultByRefNoAndResultName(refno, 'LDL').subscribe(data => {
      if (data.records != null) this.dataLDLHealthResult = data.records; else this.dataLDLHealthResult = this.generateHealthDataSet(refno, 'LDL', this.ldlMonths);
      this.dataLDLHealthResult.forEach(element => {
        this.ldlResult.push({ name: parseInt(element.month), value: parseFloat(element.result_value) });
      });
      this.xAxisLabel = 'Month';
      this.yAxisLabel = 'mmol/L';
      this.colorScheme = this.colorSchemeLDL;
      this.graphLegend = this.legendLDL;
      this.graphData = this.graphDataLDL;
      this.graphData[0].series = this.ldlResult;
      // this.customLinerChartService.showDots(this.chart);
    });
  }

  dataHbA1cHealthResult: any = [];
  hbA1cResult: any = [];
  fetchHbA1cHealthData(refno) {
    this.dataHbA1cHealthResult = [];
    this.hbA1cResult = [];
    this.graphData = [];
    this.dataService.getHealthResultByRefNoAndResultName(refno, 'HbA1c').subscribe(data => {
      if (data.records != null) this.dataHbA1cHealthResult = data.records; else this.dataHbA1cHealthResult = this.generateHealthDataSet(refno, 'HbA1c', this.hbA1cMonths);
      this.dataHbA1cHealthResult.forEach(element => {
        element.result_value = parseFloat(element.result_value).toFixed(1);
        this.hbA1cResult.push({ name: parseInt(element.month), value: element.result_value });
      });
      this.xAxisLabel = 'Month';
      this.yAxisLabel = '%';
      this.graphLegend = this.legendHbA1c;
      this.colorScheme = this.colorSchemeHbA1c;
      this.graphData = this.graphDataHbA1c;
      this.graphData[0].series = this.hbA1cResult;
      // this.customLinerChartService.showDots(this.chart);
    });
  }

  generateHealthDataSet(refno, resultName, months) {
    let arrayData = [];
    months.forEach(month => {
      arrayData.push({ refno: refno, result_name: resultName, result_value: 0.0, month: month.no });
    });
    return arrayData;
  }

  selectedPatient: any;
  openInput(template: TemplateRef<any>, data) {
    this.selectedPatient = data;
    this.fetchHbA1cHealthData(data.refno);
    this.fetchLDLHealthData(data.refno);
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });

  }

  openViewReport(template: TemplateRef<any>, data, resultName) {
    this.selectedPatient = data;
    this.selectResultName(resultName);
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-lg' });
  }

  setOneNumberDecimal($event) {

    $event.target.value = parseFloat($event.target.value).toFixed(1);
    console.log($event.target.value);
  }


  uploadResult() {
    let hbA1cResult = this.dataHbA1cHealthResult;
    let ldlResult = this.dataLDLHealthResult;
    let data = hbA1cResult.concat(ldlResult)

    this.coolDialogs.confirm("Are you sure to upload this result?")
      .subscribe(res => {
        if (res) {
          this.dataService.uploadResult(data).subscribe(data => {
            if (data.errorFound) this.toastrService.error(data.message, "Warning"); else {
              this.toastrService.success(data.message, "Success");
              this.modalRef.hide();
            }
          });
        }
      });
  }

  selectedViewResult: any;
  selectResultName(resultName) {
    this.dataHbA1cHealthResult = [];
    this.dataLDLHealthResult = [];
    this.selectedViewResult = resultName;
    if (resultName == "LDL") this.fetchLDLHealthData(this.selectedPatient.refno);
    if (resultName == "HbA1c") this.fetchHbA1cHealthData(this.selectedPatient.refno);

  }

}
