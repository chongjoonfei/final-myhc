import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { PatientRoutingModule } from './message-box-routing.module';
import { TooltipModule } from 'ngx-bootstrap';
import { MaterialModule } from '../../shared/material-module';
import { InputMessageComponent } from './input-message/input-message.component';
import { ListMessageComponent } from './inbox/inbox-message.component';
import { MessageBoxComponent } from '../message-box/message-box.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { TableModule } from 'primeng-lts/table';
import { FieldsetModule } from 'primeng-lts/fieldset';

@NgModule({
  imports: [
    CommonModule,
    PatientRoutingModule,
    SharedModule,
    ChartModule,
    TooltipModule.forRoot(),
    MaterialModule,
    AngularEditorModule,
    TableModule,
    FieldsetModule
  ],
  declarations: [
    InputMessageComponent,
    ListMessageComponent,
    MessageBoxComponent
  ]
})
export class MessageBoxModule { }
