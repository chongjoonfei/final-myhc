import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InputMessageComponent } from './input-message/input-message.component';
// import { AuthGuard } from 'src/app/service/auth/auth.guard';
import { ListMessageComponent } from './inbox/inbox-message.component';

const routes: Routes = [
 
      {
        path: '',
        component: ListMessageComponent,pathMatch: 'full'
      },
      {
        path: 'inbox',
        component: ListMessageComponent,pathMatch: 'full'
      },
      {
        path: 'input',
        component: InputMessageComponent,pathMatch: 'full'
      } 
 
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
