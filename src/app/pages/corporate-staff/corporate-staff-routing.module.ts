import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
// import { AuthGuard } from 'src/app/service/auth/auth.guard';
 
import { CorporateStaffDashboardComponent } from './dashboard/corporate-staff-dashboard.component';
 
 
const routes: Routes = [
 
      {
        path: '',
        component: CorporateStaffDashboardComponent,pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: CorporateStaffDashboardComponent,pathMatch: 'full'
      },
 
 
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorporateStaffRoutingModule { }
