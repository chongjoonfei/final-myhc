import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { DependentComponent } from './dependent.component';
 
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
 
import { MaterialModule } from '../../shared/material-module';
import { DependentDashboardComponent } from './dashboard/dependent-dashboard.component';
import { DependentRoutingModule } from './dependent-routing.module';
 

@NgModule({
  imports: [
    CommonModule,
    DependentRoutingModule,
    SharedModule,
    ChartModule,
    QRCodeSVGModule,
    TooltipModule.forRoot(),
    MaterialModule,
   
  ],
  declarations: [
    DependentDashboardComponent,
    DependentComponent,
  ]
})
export class DependentModule { }
