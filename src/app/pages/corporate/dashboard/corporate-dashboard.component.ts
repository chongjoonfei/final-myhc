import { TemplateRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { DataService } from '../../../service/data.service';

declare const AmCharts: any;
declare const $: any;

@Component({
  selector: 'app-corporate-dashboard',
  templateUrl: './corporate-dashboard.component.html',
  styleUrls: [
    './corporate-dashboard.component.scss'
  ]
})
export class CorporateDashboardComponent implements OnInit {

  
  modalRef: BsModalRef;
 
  constructor(
    private router: Router,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private coolDialogs: NgxCoolDialogsService,
    private dataService: DataService
  ) { }

  // loading=true;
  user:any;
  dataPatients:any=[];
  dataRegistration:any;
  auth:any;
  ngOnInit() {
    this.auth = JSON.parse(localStorage.getItem("auth"));
    this.dataService.getUserDetails(this.auth.username).subscribe(res=> {
      this.user = res;
      // this.dataRegistration = res.registration;
      // this.dataPatients = res.registration.registration_persons;
     
    });

        
    if (localStorage.getItem("plan-registration")){
      this.dataRegistration = JSON.parse(localStorage.getItem("plan-registration"));
      this.dataPatients = this.dataRegistration.registration_persons;
    }else{
      this.router.navigate(['/mau']);
    }
  }
 

}
