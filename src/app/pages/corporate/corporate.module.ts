import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

 
import {ChartModule} from 'angular2-chartjs';
import {SharedModule} from '../../shared/shared.module';
import { CorporateRoutingModule } from './corporate-routing.module';
import { CorporateComponent } from './corporate.component';
 
import { QRCodeSVGModule } from 'ngx-qrcode-svg';
import { TooltipModule } from 'ngx-bootstrap';
 
import { MaterialModule } from '../../shared/material-module';
import { CorporateDashboardComponent } from './dashboard/corporate-dashboard.component';
import { CompanyComponent } from './company/company.component';
 

@NgModule({
  imports: [
    CommonModule,
    CorporateRoutingModule,
    SharedModule,
    ChartModule,
    QRCodeSVGModule,
    TooltipModule.forRoot(),
    MaterialModule
  ],
  declarations: [
    CorporateDashboardComponent,
    CorporateComponent,
    CompanyComponent
  ]
})
export class CorporateModule { }
