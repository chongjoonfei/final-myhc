import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import * as _ from "lodash";
import { DataService } from '../../../../service/data.service';
import sha256 from 'crypto-js/sha256';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../../../service/auth/Authentication.service';
import { ToastrService } from 'ngx-toastr';
import { FooterColumnGroup } from 'primeng-lts/components/common/shared';


@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private dataService: DataService,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private authenticationService : AuthenticationService
     ) {
      if (localStorage.getItem('lang')) {
        this.translate.use(localStorage.getItem('lang'));
      } else {
        this.translate.setDefaultLang('en');
      }
     }
  imgSrc = 'assets/images/buttonclick_off.jpg';

  footer:any;
  ngOnInit() {
    console.log("basic-login ngOnInit");
    // localStorage.clear();
    // localStorage.clear();
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    this.formLogin = this.formBuilder.group({ "email": [null, Validators.required], "password": [null, Validators.required] });
    this.formReset = this.formBuilder.group({ "email": [null, Validators.required], "ic_no": [null, Validators.required] });
    this.formRetriveEmail = this.formBuilder.group({ "mobile_no": [null, Validators.required] });
    this.dataService.getContent("footer-login").subscribe(
      res=>{
        this.footer = res.content;
      }
    );
  }


  modalRef: BsModalRef;
  error: any;
  username: any;
  formLogin: FormGroup;
  formReset:FormGroup;
  formRetriveEmail:FormGroup;
  login() {
    if (!this.formLogin.invalid) {
      let data = this.formLogin.value;
      this.authenticationService.login(data)
      .pipe(first())
      .subscribe(
          res => {
            let auth = JSON.parse(localStorage.getItem("auth"));

            let dataAut = {
              intProject :1,
              strModuleNamePathTblName :"Login >> /login >> user_account",
              intRecordId :0,
              intAction :1, // 1=Login,2=Logout,3=Create,4=Read,5=Update,6=Delete
              strAddedByUsername :auth.account.username,
              strAddedByEmail :auth.account.email
            };
            this.dataService.createAuditrail(dataAut).subscribe(resp=>{
            },error =>{console.log(error);});

            if (auth.account.acc_type_code=='ADMIN'){
              this.router.navigate(['/administrator']);
            }else{
              this.router.navigate(['/mau']);
            }
          },
          error => {
              this.error = error.error.message;
          });

    } else {
      this.error = "Sorry, invalid email or password.";
    }
  }

  resetErrMsg(){
    this.error = null;
  }

  reset(){
    this.resetErrMsg();
    let data = this.formReset.value;
   // console.log(this.formReset);
    if (!this.formReset.invalid) {

      this.dataService.resetPassword(data)
      .subscribe(res=>{
        this.modalRef.hide();
        this.formReset.reset();
        this.toastrService.success("User password has been reset. Kindly check your Email or Phone.");
      },error=>{
        if(error.error && error.error.message){
          this.toastrService.error(error.error.message);
        }else{
          this.toastrService.error("Unable to reset password. Kindly try again shortly.");
        }
      });
    } else {
      this.toastrService.error("Sorry, invalid IC No or Email.");
    }
  }
 //
 // Retrieve Email / username
 //
 submitted = false;
 retrieveEmail(){
  this.resetErrMsg();
  let data = this.formRetriveEmail.value;

  if (!this.formRetriveEmail.invalid) {

    this.dataService.resetRetrieveEmail(data)
    .subscribe(res=>{
      console.log(res);
      this.modalRef.hide();
      this.formRetriveEmail.reset();
      this.toastrService.success("Username/Email has been retrieved. Kindly check your Email or Phone.");
    },error=>{
      if(error.error && error.error.message){
        this.toastrService.error(error.error.message);
      }else{
        this.toastrService.error("Unable to retrieve Username/Email. Kindly try again shortly.");
      }
    });
  } else {
    this.toastrService.error("Sorry, invalid Mobile No.");
  }
 }






  forgotPassword(template: TemplateRef<any>){
    //console.log(template);
    this.resetErrMsg();
    this.formReset.controls
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }


  forgotEmail(template: TemplateRef<any>){
    //console.log(template);
    this.resetErrMsg();
    this.formReset.controls
    this.modalRef = this.modalService.show(template, { backdrop: 'static', class: 'gray modal-md' });
  }
}
