import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../../../service/data.service';
import * as _ from "lodash";
import { MatStepper } from '@angular/material';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { environment } from '../../../../../../src/environments/environment';
import { DatePipe } from '@angular/common';
import { ConfirmationModalService } from '../../../../shared/modal/confirmation-dialog.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-basic-reg',
  templateUrl: './basic-reg.component.html',
  styleUrls: ['./basic-reg.component.scss']
})
export class BasicRegComponent implements OnInit {
  isLinear = false;
  formDetails: FormGroup;
  formNCD: FormGroup;
  submitted = false;
  citizen = true;
  icCardPanel = false;
  passportPanel = false;


  constructor(
    private router: Router,
    private dataService: DataService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private modalService: BsModalService,
    private toastrService: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private datePipe: DatePipe,
    private confirmationModalService: ConfirmationModalService
  ) {
    if (localStorage.getItem('lang')) {
      this.translate.use(localStorage.getItem('lang'));
    } else {
      this.translate.setDefaultLang('en');
    }
  }

  language: any;
  footer:any;
  screeningPlans: any = [];
  selectedPlan: any;
  isVerifiedDependent = false;
  relationshipPanel = false;
  sevenDays: any = [{ no: 0, text: "Don't know" },
  { no: 1, text: "1 Day" }, { no: 2, text: "2 Days" }, { no: 3, text: "3 Days" }, { no: 4, text: "4 Days" },
  { no: 5, text: "5 Days" }, { no: 6, text: "6 Days" }, { no: 7, text: "7 Days" }]

  packageCategories: any;
  selectedPackageCategory: any;
  localhost: any;
  groupPlanPackages: any;

  ngOnInit() {
    //  let mobile_no_real = "60173019855";

    //  this.dataService.get6DigitCode(mobile_no_real).subscribe(sixDigit => {
    // // //  console.log(sixDigit.code);
    //   let input = { "code": "six-digit-code", "mobile": mobile_no_real, "paramValues": [{ "param": "?code", "value": sixDigit.code }] };
    //     this.dataService.sendSms(input).subscribe(msg => {

    //        console.log(msg);
    //      });
    //    });

    if (localStorage.getItem('lang')) {
      this.translate.use(localStorage.getItem('lang'));
      this.language = localStorage.getItem('lang');
    } else {
      this.translate.setDefaultLang('en');
      this.language = 'en';
    }

    this.icCardPanel = true;

    window["scope"]=this;
    this.localhost = window.location.protocol + "//" + window.location.host;
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');


    this.initFormDetails();
    // this.dataService.getAllScreeningPlan().subscribe(data => {
    //   let orderedPlans = _.orderBy(data, ['category_code'], ['asc']);
    //   let plans = ['SP01','SP03','SP05','SP07'];
    //   plans.forEach(element => {
    //     console.log(element)
    //     let data = _.filter(orderedPlans, {package_code:element})
    //     if (data.length>0) this.screeningPlans.push(data[0]);
    //   });
    //   console.log(this.screeningPlans);
    // });

    // this.dataService.getAllPackageCategoryShowStatus().subscribe(result => {
    //   this.packageCategories = result.data;
    // });

    this.dataService.getScreenPlanByCommercial('YES').subscribe(result => {
      // this.planPackages = result.data;
      let groupPlanPackages = [];
      let plans = ['FREE1-ID','FREE2-DC','FREE3-DE','FREE4-FM'];
      let catPlans = {'P1INDIVIDUAL':[], 'P2DEPENDENT':[], 'P3FAMILY':[]};
      plans.forEach(plan => {
        let data = _.filter(result.data, {package_code:plan})
        if (data.length>0) {
          groupPlanPackages.push(data[0]);
          if (data[0].category_code=='INDIVIDUAL')
            catPlans.P1INDIVIDUAL.push(data[0]);
          else if (data[0].category_code=='FAMILY')
            catPlans.P3FAMILY.push(data[0]);
          else if (data[0].category_code=='DEPENDENT')
            catPlans.P2DEPENDENT.push(data[0]);
        }
      });
      this.groupPlanPackages = catPlans;

      //Get all cities
      this.getCities();

    });

    //this.initFormNCD();

    this.dataService.getContent("footer-page").subscribe(
      res=>{
        this.footer = res.content;
      }
    );

    this.fetchlookupInfo();
  }

  changeLanguage(language) {
    localStorage.setItem('lang', language);

    this.translate.use(localStorage.getItem('lang'));
    this.ngOnInit();
  }


  selectedIndex: number = 0;

  setIndex(event) {
    this.selectedIndex = event.selectedIndex;
  }

  triggerClick(event) {
    console.log(`Selected tab index: ${this.selectedIndex}`);
  }

  isCitizen(event) {
    if (event.currentTarget.checked == true) {
      this.icCardPanel = true;
      this.passportPanel = false;
    } else {
      this.icCardPanel = false;
      this.passportPanel = true;
    };
  }

  optPackageCategory: any = null;
  startOver() {
    this.stepper.reset();
    this.selectedPackageCategory = null;
    this.selectedPlan = null;
    this.sixDigitCode = null;
    this.optPackageCategory = null;
    this.initFormDetails();
  }

  previous() {
    this.stepper.selected.completed = true;
    this.stepper.previous();
  }

  // planPackages: any;
  onPackageCategoryOptChange(pCategory) {
    this.selectedPackageCategory = pCategory;
    // this.dataService.getScreenPlanByCommercialCategory(pCategory,'YES').subscribe(res=>{
    //   this.planPackages = res.data;
    // });

    // this.planPackages = _.filter(this.screeningPlans, { category_code: pCategory });
  }

  onPackagePlanOptChange(plan) {
    this.selectedPlan = plan;
  }

  getSelectedPlan(plan) {
    if (plan == 'FREE1-ID') {
      this.relationshipPanel = false;
    } else {
      this.relationshipPanel = true;
    }
  }

  verifyTypeRegistration() {
    if (this.selectedPlan) {
      this.stepper.selected.completed = true;
      this.stepper.next();
    } else {
      this.stepper.selected.completed = false;
      this.translate.get('registration').subscribe(data => {
        this.toastrService.warning(data.notification.title_step1, data.notification.title_warning);
      });

    }
  }

  dependentIcNo: any;
  errorDependent: any;
  // verifyDependentIcNo(){
  //   this.errorDependent = null;
  //   this.isVerifiedDependent=false;
  //   this.dataService.verifyIcNoDependent(this.dependentIcNo).subscribe(res=>{
  //     // console.log(res);
  //     this.isVerifiedDependent = res.errorFound;
  //     if (res.errorFound==true){
  //       this.errorDependent = "invalid";
  //       this.stepper.selected.completed = false;
  //       this.isVerifiedDependent = false;
  //       this.toastrService.warning(res.message, "Warning");
  //     }else{
  //       this.errorDependent=null;
  //       this.isVerifiedDependent=true;
  //       this.toastrService.success(res.message, "Success");
  //     }

  //   });
  // }


  states: any;
  relationships: any;
  fetchlookupInfo() {
    this.dataService.getStates().subscribe(res => {
      this.states = res.data;
    });
    this.dataService.getRelationships().subscribe(res => {
      this.relationships = res.data;
    });
  }

  cities: any;
  getCities() {
    this.dataService.getCities().subscribe(res => {
      this.cities = res.data;
    });
  }

  selectedCity= "";
  getSelectedCity(postcode) {
    this.dataService.getSelectedCity(postcode).subscribe(res => {
      this.selectedCity = res[0].post_office;
    }, error => {
      this.translate.get('registration').subscribe(data => {
        this.toastrService.error(data.notification.title_errorCity, data.notification.title_warning);
      });
    });
  }

  initFormDetails() {
    this.formDetails = this.formBuilder.group({
      name: [null, Validators.required],
      // password: null,
      ic_no: [null, Validators.required],

      //ic_no: null,
     // passport:[null, Validators.required],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      mobile_no: [null, Validators.required],
      citizen: null,
      race: null,
      religion: null,
      dob: ['', Validators.required],
      marital: null,
      gender: null,
      package_code: null,
      address: ['', Validators.required],
      town: ['', Validators.required],
      district: [''],
      postcode: ['', Validators.required],
      state: ['', Validators.required],
      amount_paid: 0.00,
      payment_no: null,
      payment_method: null,
      payment_date: null,
      date_registered: null,
      date_expired: null,
      status: 'PRE-ACCOUNT',
      center_code: null,
      chk_tnc: [null]
    });
  }

  get fDetail() { return this.formDetails.controls; }

  verifyDetails() {
    this.submitted = true;

    if (this.formDetails.invalid) {
      return;
    }


    this.dataService.validateIC(this.formDetails.value.ic_no.replace(/-/g, "")).subscribe(res => {
     // console.log("Reeve" + res + this.formDetails.value.ic_no);
      if (res == 1) {
        this.stepper.selected.completed = false;
        this.translate.get('registration').subscribe(data => {
          this.toastrService.warning(data.notification.title_warning_step2, data.notification.title_warning);
        });
      } else {
        this.translate.get('registration').subscribe(data => {
          this.confirmationModalService.confirm(
            data.notification.title_step2
          ).pipe(
            take(1)
          ).subscribe(result => {
           // console.log(result + " >>"+ "LalaReeve" );
            if (result) {
              this.stepper.selected.completed = true;
              this.stepper.next();
            }
          });
        });
      }
    });
  }


  /*
  initFormNCD() {
    // console.log(this.formDetails.value);
    this.formNCD = this.formBuilder.group({
      ic_no: [this.formDetails.get("ic_no").value],
      tabacco_use: [null, [Validators.required]],
      alcohol_consumption: [null, [Validators.required]],
      diet_eat_fruit: [null, [Validators.required]],
      diet_eat_vege: [null, [Validators.required]],
      physical_activities: [null, [Validators.required]],
      history_hypertension: [null, [Validators.required]],
      history_diabetes: [null, [Validators.required]]
    });
  }

  verifyFormNCD() {
    if (!this.formNCD.invalid) {
      this.translate.get('registration').subscribe(data => {
        this.confirmationModalService.confirm(
          data.notification.title_step3
        ).pipe(
          take(1)
        ).subscribe(result => {
          if (result) {
            this.stepper.selected.completed = true;
            this.stepper.next();
          }
        });
      });

    } else {
      this.stepper.selected.completed = false;
      this.translate.get('registration').subscribe(data => {
        this.toastrService.warning(data.notification.title_warning_step3, data.notification.title_warning);
      });
    }
  }
  */

  isSendingSMS:any=false;
  checkTermCondition: any;
  check3rdPartyUpdate:any=false;
  checkSendUpdates:any=false;
  sentSmsSixDigitCode() {

    if (this.checkTermCondition) {
      this.isSendingSMS = false;
      let mobile_no = "60" + this.formDetails.get("mobile_no").value;

      this.dataService.get6DigitCode(mobile_no).subscribe(sixDigit => {
         let input = { "code": "six-digit-code", "mobile": mobile_no, "paramValues": [{ "param": "?code", "value": sixDigit.code }] };


    //     this.translate.get('registration').subscribe(data => {
    //       this.toastrService.warning('SMS Notification has been sent to your mobile phone : code ::'+ sixDigit.code, data.notification.title_success);
    //     });
    //     this.stepper.selected.completed = true;
    //         this.stepper.next();
    //   });
    // }

        this.dataService.sendSms(input).subscribe(msg => {
          if (!msg.errorFound) {
            this.translate.get('registration').subscribe(data => {
              this.toastrService.warning(msg.message, data.notification.title_success);
            });
            this.stepper.selected.completed = true;
            this.stepper.next();
          } else {
            this.translate.get('registration').subscribe(data => {
              this.toastrService.warning(msg.message, data.notification.title_fail);
            });
          }
          this.isSendingSMS=false;
        }, err => {
          this.isSendingSMS=false;
          this.translate.get('registration').subscribe(data => {
            this.toastrService.error(data.notification.title_mobile_invalid + mobile_no + data.notification.title_invalid, err.error.error);
          });
        });
      }, error => {
        this.isSendingSMS=false;
        this.translate.get('registration').subscribe(data => {
          this.toastrService.error(data.notification.title_process, data.notification.title_fail);
        });
      });
    } else {
      this.isSendingSMS=false;
      this.stepper.selected.completed = false;
      this.translate.get('registration').subscribe(data => {
        this.toastrService.warning(data.notification.title_warining_step4, data.notification.title_warning);
      });
    }

  }

  isRegistering: any = false;
  sixDigitCode: any;
  codeConfirmation = false;
  @ViewChild('stepper', { static: false }) private stepper: MatStepper;
  verifyCode() {
    this.isRegistering = true;

    let mobile_no = "60" + this.formDetails.get("mobile_no").value;
    let input = { "code": this.sixDigitCode, "mobile": mobile_no };
    this.dataService.verify6DigitCode(input).subscribe(verification => {
      this.translate.get('registration').subscribe(data => {
        this.toastrService.success(verification.message, data.notification.title_success);
      });
      let data = this.formDetails.value;


      data.package_code = this.selectedPlan;
      data.amount_paid = "0.00";
      data.date_registered = this.datePipe.transform(new Date, 'yyyy-MM-dd hh:mm a');

      data.dob = this.datePipe.transform(data.dob, 'yyyy-MM-dd hh:mm a');
      data.chk_tnc = this.checkTermCondition;
      data.citizen = this.formDetails.value.citizen == '' ? 'false' : 'true';
      console.log(data);
      //this.formNCD.get('ic_no').setValue(data.ic_no);
     // data.ncd = this.formNCD.value;
      //add terms checkbox

      this.dataService.preRegister(data).subscribe(result => {
        this.isRegistering = false;
        this.stepper.selected.completed = true;
        this.codeConfirmation = true;
        this.stepper.next();
        //add terms checkbox
        this.dataService.createDataSharing({
          ic_no:data.ic_no,
          bmz_updates:this.checkSendUpdates?"true":"false",
          partner_updates:this.check3rdPartyUpdate?"true":"false"
        }).subscribe(e=>{
            console.log(e);
        })

      }, err => {
        this.isRegistering = false;
        this.translate.get('registration').subscribe(data => {
          this.toastrService.error(err.error.message, data.notification.title_warning);
        });
        this.translate.get('registration').subscribe(data => {
          this.toastrService.error(err.error.message, data.notification.title_registration_fail);
        });
      });

    }, verification => {
      if (verification.error.errorFound) {
        if (verification.error.message.indexOf("expired") != -1) {
          this.toastrService.error(verification.error.message, verification.error.error);
        } else {
          this.translate.get('registration').subscribe(data => {
            this.toastrService.error(data.notification.title_invalid_code_desc, data.notification.title_invalid_code);
          });
        }
      }
      this.stepper.selected.completed = false;
      this.codeConfirmation = false;
      this.isRegistering = false;
    });

  }
  goToLoginPage() {
    this.router.navigate(['/', 'login']);
  }

}
