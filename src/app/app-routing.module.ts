import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent} from './layout/main/main.component';
import { AuthComponent} from './layout/auth/auth.component';
import { MauPageComponent } from './pages/mau-page/mau-page.component';
import { MyDashboardComponent } from './pages/my-dashboard/my-dashboard.component';
import { MyBiodataComponent } from './pages/my-biodata/my-biodata.component';
import { MyQrCodeComponent } from './pages/my-qr-code/my-qr-code.component';
import { ErrorComponent } from './pages/error/error.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { MyAssessmentsComponent } from './pages/assessments/my-assessments/my-assessments.component';
import { MyRecordsComponent } from './pages/records/my-records/my-records.component';
import { MyBookingComponent } from './pages/my-booking/my-booking.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        loadChildren: () => import('./pages/auth/login/basic-login/basic-login.module').then(m => m.BasicLoginModule)
      }
      ,
      {
        path: 'login/:account-type',
        loadChildren: () => import('./pages/auth/login/basic-login/basic-login.module').then(m => m.BasicLoginModule)
      }
      ,
      {
        path: 'register',
        loadChildren: () => import('./pages/auth/registration/basic-reg/basic-reg.module').then(m => m.BasicRegModule)
      }
    ]
  },
  {
    path: 'mau',
    component: MauPageComponent,
    children: [
      {
        path: 'main',
        loadChildren: () => import('./pages/mau-page/mau-page.module').then(m => m.MauPageModule)
      },
    ]
  },
  {
    path: '',
    component: MainComponent,
    children: [
      // {
      //   path: 'patient',
      //   loadChildren: () => import('./pages/patient/patient.module').then(m => m.PatientModule)
      // },
      // {
      //   path: 'family',
      //   loadChildren: () => import('./pages/patient/patient.module').then(m => m.PatientModule)
      // },
      // {
      //   path: 'individual',
      //   loadChildren: () => import('./pages/patient/patient.module').then(m => m.PatientModule)
      // },
      // {
      //   path: 'dependent',
      //   loadChildren: () => import('./pages/dependent/dependent.module').then(m => m.DependentModule)
      // },
      // {
      //   path: 'corporate',
      //   loadChildren: () => import('./pages/corporate/corporate.module').then(m => m.CorporateModule)
      // },
      // {
      //   path: 'corporate-staff',
      //   loadChildren: () => import('./pages/corporate-staff/corporate-staff.module').then(m => m.CorporateStaffModule)
      // },
      {
        path: 'administrator',
        loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule)
      },
      {
        path: 'test-panel',
        loadChildren: () => import('./pages/test-panel/test-panel.module').then(m => m.TestPanelModule)
      },
      {
        path: 'add-on',
        loadChildren: () => import('./pages/add-on/add-on.module').then(m => m.AddOnPurchaseModule)
      },
      {
        path: 'contact-support',
        loadChildren: () => import('./pages/contact/contact-support.module').then(m => m.ContactSupportModule)
      },
      {
        path: 'message-box',
        loadChildren: () => import('./pages/message-box/message-box.module').then(m => m.MessageBoxModule)
      },
      {
        path: 'marketplace',
        loadChildren: () => import('./pages/marketplace/marketplace.module').then(m => m.MarketplaceModule)
      },
      {
        path: 'my-dashboard/main',
        component: MyDashboardComponent,pathMatch: 'full'
      },
      {
        path: 'my-biodata/main',
        component: MyBiodataComponent,pathMatch: 'full'
      },
      {
        path: 'my-qr-code/main',
        component: MyQrCodeComponent,pathMatch: 'full'
      },
      {
        path: 'my-account/main',
        component: MyAccountComponent,pathMatch: 'full'
      },
      {
        path: 'my-booking/booking',
        component: MyBookingComponent,pathMatch: 'full'
      },
      {
        path: 'assessments/my-assessments',
        component: MyAssessmentsComponent,pathMatch: 'full'
      },
      {
        path: 'records/my-records',
        component: MyRecordsComponent,pathMatch: 'full'
      },
      {
        path: 'error/:code/:ref',
        component: ErrorComponent,pathMatch: 'full'
      },
    ]
  },

];


@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
