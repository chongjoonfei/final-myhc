// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   localhost: 'https://myhc.my/',
//   uploadPath: '../../upload_files/',
//   apiUrl: 'https://myhc.my/myhc-api'
// };


export const environment = {
  production: false,
  localhost: 'http://127.0.0.1',
  uploadPath: '../../upload_files/',
  apiUrl: 'http://127.0.0.1/myhc-api',
};