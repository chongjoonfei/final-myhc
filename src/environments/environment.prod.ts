export const environment = {
  production: true,
  localhost: 'http://dev2.myhc.my/',
  uploadPath: '../../upload_files/',
  apiUrl: 'http://dev2.myhc.my/myhc-api-dev'
};
